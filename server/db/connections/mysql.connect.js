const mysql = require('mysql')
    , con = mysql.createConnection({
      host: "127.0.0.1",
      // host:"13.232.6.202",
      user: "root",
      password: "1234",
      database: "staples_db",
      port:"3306"
    });

module.exports = con;

// con.connect(function(err) {
//   if (err) throw err;
//   console.log("Connected!");
//   /*Create a table named "customers":*/
//   var sql = "CREATE TABLE customers (name VARCHAR(255), address VARCHAR(255))";
//   con.query(sql, function (err, result) {
//     if (err) throw err;
//     console.log("Table created");
//   });
// });
