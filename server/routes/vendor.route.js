// const vo = require('express').Router(),
//   request = require('superagent'),
//   con = require('./../db/connections/mysql.connect.js');
// var vendorId=' ';
// vo.post('/vendorRequest', (req, res) => {
//   console.log('inside route ', req.query);
//   con.query("SELECT vendor_id FROM vendor_details", function(err,result,fields){
//     if(err) throw err;
//     else{
//       console.log(result.length);
//       console.log('type of vendor id',result[result.length-1].vendor_id);
//
//       if(result.length == null){
//         for(var i = 100 ; i>=100 ; i++){
//           i = i + 1;
//           vendorId = 'VE' + i;
//         }
//       }
//       else{
//         var firstletter = result[result.length-1].vendor_id.slice(0,2)
//         var secondLetter = parseInt(result[result.length-1].vendor_id.slice(2))
//          var last = secondLetter + 1;
//          vendorId = firstletter + last;
//       }
//       console.log('vendor--------------->',vendorId);
//     }
//     console.log('catrgory order details',category_details,req.query.vendor_category.category);
//     var rating = (Math.random() * (3.5 - 5.0) + 5.0).toFixed(1);
//     if(req.query.vendor_category.category == 'AC Maintenance'){
//       con.query('INSERT INTO vendor_details (vendor_name, vendor_id, vendor_location, service_category, rating, charge_details, distance_served) values("'+req.query.vendor_name+'", "'+vendorId+'","'+req.query.vendor_location+'", "'+req.query.vendor_category.category+'", "'+rating+'", "'+req.query.vendor_category.AreaInSqFt+'", "'+req.query.vendor_distance+'")',function(err,res){
//         if(err)
//         console.log(err);
//         else{
//           console.log(res);
//         }
//       });
//
//
//     }
//     else{
//       var category_details = req.query.vendor_category.Bathroom +'-'+req.query.vendor_category.Pantry+'-'+req.query.vendor_category.Window+'-'+req.query.vendor_category.AdditionalSqFt;
//
//       con.query('INSERT INTO vendor_details (vendor_name, vendor_id, vendor_location, service_category, rating, charge_details, distance_served) values("'+req.query.vendor_name+'", "'+vendorId+'","'+req.query.vendor_location+'", "'+req.query.vendor_category.category+'", "'+rating+'", "'+category_details+'", "'+req.query.vendor_distance+'")',function(err,res){
//         if(err)
//         console.log(err);
//         else{
//           console.log(res);
//         }
//       });
// }
// });
//
// });
//
// module.exports = vo;
const vo = require('express').Router(),
  request = require('superagent'),
  con = require('./../db/connections/mysql.connect.js');

let vendorId = ' ';
vo.post('/vendorRequest', (req, res) => {
  console.log('inside route ', req.query);
  con.query('SELECT vendor_id FROM vendor_details', (err, result, fields) => {
    if (err) throw err;
    else {
      console.log(result.length);
      console.log('type of vendor id', result[result.length - 1].vendor_id);

      if (result.length == null) {
        for (let i = 100; i >= 100; i++) {
          i += 1;
          vendorId = `VE${i}`;
        }
      } else {
        const firstletter = result[result.length - 1].vendor_id.slice(0, 2);
        const secondLetter = parseInt(result[result.length - 1].vendor_id.slice(2));
        const last = secondLetter + 1;
        vendorId = firstletter + last;
      }
      console.log('vendor--------------->', vendorId);
    }
    console.log('catrgory order details', category_details, req.query.vendor_category.category);
    const rating = (Math.random() * (3.5 - 5.0) + 5.0).toFixed(1);
    if (req.query.vendor_category.category == 'Duct Cleaning') {
      con.query(
        `INSERT INTO vendor_details (vendor_name, vendor_id, vendor_location, service_category, rating, charge_details, distance_served, vendor_status) values("${
          req.query.vendor_name
        }", "${vendorId}","${req.query.vendor_location}", "${
          req.query.vendor_category.category
        }", "${rating}", "${req.query.vendor_category.AreaInSqFt}", "${
          req.query.vendor_distance
        }", "${req.query.vendor_status}")`,
        (err, res) => {
          if (err) console.log(err);
          else {
            console.log(res);
          }
        },
      );
    } else {
      var category_details = `${req.query.vendor_category.Bathroom}-${
        req.query.vendor_category.Pantry
      }-${req.query.vendor_category.Window}-${req.query.vendor_category.AdditionalSqFt}`;

      con.query(
        `INSERT INTO vendor_details (vendor_name, vendor_id, vendor_location, service_category, rating, charge_details, distance_served, vendor_status) values("${
          req.query.vendor_name
        }", "${vendorId}","${req.query.vendor_location}", "${
          req.query.vendor_category.category
        }", "${rating}", "${category_details}", "${req.query.vendor_distance}", "${
          req.query.vendor_status
        }")`,
        (err, res) => {
          if (err) console.log(err);
          else {
            console.log(res);
          }
        },
      );
    }
  });
});

module.exports = vo;
