const router = require('express').Router();

router.get('/allCategories', (req, res) => {
  console.log('category router');
  // res.send("categories")
  res.send(JSON.stringify([
    {
      Id: 1,
      SubscriberId: 123,
      HasDependency: true,
      PrimaryCategoryID: 'SV101',
      image_src: './client/Images/mop1.png',
      PrimaryCategoryName: 'Office Cleaning',
      tags: ['cleaning', 'deep cleaning','full cleaning','pantry', 'pantry cleaning','carpet cleaning','window cleaning','bathroom cleaning','complete cleaning']
    },
    {
      Id: 2,
      SubscriberId: 123,
      HasDependency: true,
      PrimaryCategoryID: 'SV102',
      image_src: './client/Images/maintenance1.png',
      PrimaryCategoryName: 'Plumbing',
      tags: ['plumbing','pipe','fixing','fitting']
    },
    {
      Id: 3,
      SubscriberId: 123,
      HasDependency: true,
      PrimaryCategoryID: 'SV103',
      image_src: './client/Images/computer.png',
      PrimaryCategoryName: 'IT Services',
      tags: ['it', 'it services']
    },
    {
      Id: 4,
      SubscriberId: 123,
      HasDependency: true,
      PrimaryCategoryID: 'SV104',
      image_src: './client/Images/air-conditioner2.png',
      PrimaryCategoryName: 'A/C Service',
      tags: ['ac', 'a/c', 'ac service', 'full cleaning', 'duct cleaning']
    },
    {
      Id: 5,
      SubscriberId: 123,
      HasDependency: true,
      PrimaryCategoryID: 'SV105',
      image_src: './client/Images/desk.png',
      PrimaryCategoryName: 'Furniture Services',
      tags: ['furniture', 'chairs', 'tables']
    },
    {
      Id: 6,
      SubscriberId: 123,
      HasDependency: true,
      PrimaryCategoryID: 'SV106',
      image_src: './client/Images/lorry1.png',
      PrimaryCategoryName: 'Moving Office',
      tags: ['shifting','packaging','office', 'moving']
    },
    {
      Id: 7,
      SubscriberId: 123,
      HasDependency: true,
      PrimaryCategoryID: 'SV106',
      image_src: './client/Images/printer.png',
      PrimaryCategoryName: 'Manage Print Services',
      tags: ['printer','print', 'service']
    },
    {
      Id: 8,
      SubscriberId: 123,
      HasDependency: true,
      PrimaryCategoryID: 'SV106',
      image_src: './client/Images/owner.png',
      PrimaryCategoryName: 'FMG Services',
      tags: ['fmg', 'facility', 'services']
    },
    {
      Id: 9,
      SubscriberId: 123,
      HasDependency: true,
      PrimaryCategoryID: 'SV106',
      image_src: './client/Images/pencil.png',
      PrimaryCategoryName: 'Branding Service',
      tags: ['brand', 'branding', 'services']
    },
  ]));
});
router.post('/createWorkOrder', (req, res) => {
  res.send('OrderID_13477543');
});
router.get('/allworkorders', (req, res) => {
  res.send(JSON.stringify([
    {
      CompanyName: 'XYZ',
      OrderId: 'OR_1011',
      Status: 'Submitted',
      Submitted_date: '3 Apr 2018',
      Assignee: 'All Seasons',
      CompanyId: 'CM100023',
      CompanyAddress: '1e Glanshof 1-15	1103SJ	Amsterdam',
    },
    {
      CompanyName: 'XYZ',
      OrderId: 'OR_1012',
      Status: 'Submitted',
      Submitted_date: '3 Apr 2018',
      Assignee: 'All Seasons',
      CompanyId: 'CM100023',
      CompanyAddress: '1e Glanshof 1-15	1103SJ	Amsterdam',
    },
    {
      CompanyName: 'XYZ',
      OrderId: 'OR_1013',
      Status: 'Inprogress',
      Submitted_date: '15 Mar 2018',
      Assignee: 'JOANN',
      CompanyId: 'CM100023',
      CompanyAddress: '1e Glanshof 1-15	1103SJ	Amsterdam',
    },
    {
      CompanyName: 'XYZ',
      OrderId: 'OR_1014',
      Status: 'Completed',
      Submitted_date: '3 Mar 2018',
      Assignee: 'Hertz',
      CompanyId: 'CM100023',
      CompanyAddress: '1e Glanshof 1-15	1103SJ	Amsterdam',
    },
    {
      CompanyName: 'XYZ',
      OrderId: 'OR_1015',
      Status: 'Inprogress',
      Submitted_date: '24 Mar 2018',
      Assignee: 'nexTech',
      CompanyId: 'CM100023',
      CompanyAddress: '1e Glanshof 1-15	1103SJ	Amsterdam',
    },
    {
      CompanyName: 'XYZ',
      OrderId: 'OR_1016',
      Status: 'Completed',
      Submitted_date: '2 Mar 2018',
      Assignee: 'CURO',
      CompanyId: 'CM100023',
      CompanyAddress: '1e Glanshof 1-15	1103SJ	Amsterdam',
    },
    {
      CompanyName: 'XYZ',
      OrderId: 'OR_1017',
      Status: 'Submitted',
      Submitted_date: '3 Apr 2018',
      Assignee: 'ACS',
      CompanyId: 'CM100023',
      CompanyAddress: '1e Glanshof 1-15	1103SJ	Amsterdam',
    },
    {
      OrderId: 'OR_1018',
      Status: 'Completed',
      Submitted_date: '3 Mar 2018',
      Assignee: 'nexTech',
      CompanyId: 'CM100020',
    },
    {
      OrderId: 'OR_1018',
      Status: 'Inprogress',
      Submitted_date: '3 Mar 2018',
      Assignee: 'M.C. Wardrobe Consultant',
      CompanyId: 'CM100020',
    },
    {
      OrderId: 'OR_1019',
      Status: 'Submitted',
      Submitted_date: '3 Apr 2018',
      Assignee: 'M.C. Wardrobe Consultant',
      CompanyId: 'CM100020',
    },
    {
      OrderId: 'OR_1020',
      Status: 'Completed',
      Submitted_date: '2 Mar 2018',
      Assignee: 'Angel Cleaning Service',
      CompanyId: 'CM100020',
    },
    {
      OrderId: 'OR_1021',
      Status: 'Completed',
      Submitted_date: '3 Mar 2018',
      Assignee: 'Angel Cleaning Service',
      CompanyId: 'CM100020',
    },
    {
      OrderId: 'OR_1022',
      Status: 'Inprogress',
      Submitted_date: '5 Mar 2018',
      Assignee: 'Busy Bees Cleaning Service',
      CompanyId: 'CM100020',
    },
    {
      OrderId: 'OR_1023',
      Status: 'Completed',
      Submitted_date: '3 Mar 2018',
      Assignee: 'Busy Bees Cleaning Service',
      CompanyId: 'CM100020',
    },
    {
      OrderId: 'OR_1024',
      Status: 'Submitted',
      Submitted_date: '3 Apr 2018',
      Assignee: 'Caring Cleaners',
      CompanyId: 'CM100018',
    },
    {
      OrderId: 'OR_10251',
      Status: 'Inprogress',
      Submitted_date: '4 Mar 2018',
      Assignee: 'Orlando cleaning company',
      CompanyId: 'CM100018',
    },
    {
      OrderId: 'OR_10252',
      Status: 'Completed',
      Submitted_date: '26 Mar 2018',
      Assignee: 'Orlando cleaning company',
      CompanyId: 'CM100018',
    },
    {
      OrderId: 'OR_10253',
      Status: 'Submitted',
      Submitted_date: '3 Apr 2018',
      Assignee: 'Checklist Cleaning',
      CompanyId: 'CM100018',
    },
    {
      OrderId: 'OR_10254',
      Status: 'Inprogress',
      Submitted_date: '1 Mar 2018',
      Assignee: 'Company NA',
      CompanyId: 'CM100018',
    },
    {
      OrderId: 'OR_10255',
      Status: 'Completed',
      Submitted_date: '2 Mar 2018',
      Assignee: 'Company NA',
      CompanyId: 'CM100018',
    },
    {
      OrderId: 'OR_10256',
      Status: 'Inprogress',
      Submitted_date: '1 Mar 2018',
      Assignee: 'Orlando cleaning company',
      CompanyId: 'CM100018',
    },
    {
      OrderId: 'OR_10257',
      Status: 'Completed',
      Submitted_date: '22 Mar 2018',
      Assignee: 'Infinity Cleaners',
      CompanyId: 'CM100010',
    },
    {
      OrderId: 'OR_10258',
      Status: 'Submitted',
      Submitted_date: '3 Apr 2018',
      Assignee: 'Infinity Cleaners',
      CompanyId: 'CM100010',
    },
    {
      OrderId: 'OR_10259',
      Status: 'Completed',
      Submitted_date: '29 Mar 2018',
      Assignee: 'Carfagno',
      CompanyId: 'CM100010',
    },
    {
      OrderId: 'OR_10260',
      Status: 'Submitted',
      Submitted_date: '3 Apr 2018',
      Assignee: "Hoyt's Home Improvement",
      CompanyId: 'CM100010',
    },
    {
      OrderId: 'OR_10261',
      Status: 'Inprogress',
      Submitted_date: '25 Mar 2018',
      Assignee: "Hoyt's Home Improvement",
      CompanyId: 'CM100010',
    },
    {
      OrderId: 'OR_10262',
      Status: 'Completed',
      Submitted_date: '21 Mar 2018',
      Assignee: 'All Seasons',
      CompanyId: 'CM100010',
    },
  ]));
});
router.get('/retrieveWorkOrder', (req, res) => {
  res.send(JSON.stringify([
    {
      CompanyName: 'Hertz',
      CompanyAddress1: 'Building E,10th Floor,Reoterssart 1018WB,Amsterdam',
      Country: ' Netherlands',
      State: 'Amsterdam',
      City: 'Amsterdam',
      Zip: '1011 AC',
      Phone: '+31 3958723084',
      Fax: '865246',
      ContactName: 'Anne',
      orderId: 'OR_1013',
      IsCheckInDenied: false,
      CheckInDeniedReason: 'no',
      CheckInRange: 0,
      RecallWorkOrder: 0,
      IsInvoiced: true,
      IsExpired: true,
      IsEnabledForMobile: true,
      OrderSubmitted: '1 Mar 2018',
      ServiceRequested: '22 Apr 2018',
      timeSlot: '17:00 to 22:00hrs',
      ServiceType: 'Deep Cleaning',
      TotalCharge: '€700',
      ClosingAuthorizationNumber: '243255',
      CallerId: 10303,
      AutoComplete: true,
      AutoInvoice: true,
      VendorUserId: 12344,
      VendorUserName: 'Anne',
      VendorAddress: 'Hoogoorddreef 62, 1101 BE Amsterdam-Zuidoos',
      VendorContact: '+31837576234',
    },
  ]));
});

module.exports = router;
