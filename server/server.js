const express = require('express'),
  app = express(),
  PORT = process.env.PORT || 3333,
  con = require('./db/connections/mysql.connect.js'),
  server = require("http").Server(app);

const serviceRequest = require('./routes/serviceRequest.route.js');
const routes = require('./routes/category.route.js');
const vendorRoute = require('./routes/vendor.route.js');
const serviceOrder = require('./routes/serviceOrder.route.js');
const searchbyOrderId = require('./routes/searchbyOrderId.route.js');

app.use('/', (req, res, next) => {
  console.log('inside routes');
  next();
},routes, serviceRequest, vendorRoute, serviceOrder, searchbyOrderId);


app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use(express.static('./../'));

server.listen(PORT, (err, res) => {
  if (err) {
    console.log('error in server - > ', err);
  } else {
    console.log('Server up and listening on port ', PORT);
  }
});
