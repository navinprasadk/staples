import React, { Component } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import Signin from './components/Signin.jsx';
import Signup from './components/Signup.jsx';
import Home from './components/Home.jsx';
import SigninStaples from './components/SigninStaples.jsx';
import CleaningDetails from './components/CleaningDetails.jsx';
import ConfirmRegistered from './components/ConfirmRegistered.jsx';
import Subcategories from './components/Subcategories.jsx';
import Ordersearch from './components/OrderSearch.jsx';
import PageNotFound from './components/PageNotFound.jsx';
import CompanySearch from './components/CompanySearch.jsx';
import OrderIDSearch from './components/OrderIdSearch.jsx';
import Allorders from './components/AllOrders.jsx';
import VendorRegistration from './components/VendorRegistration.jsx';
import ConfirmationGuest from './components/ConfirmationGuest.jsx';
import AcCleaning from './components/AcCleaning.jsx';
import AcConfirmPage from './components/AcConfirmPage.jsx';
import MyOrders from './components/myOrders.jsx';

export default class AppRouter extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <HashRouter>
        <Switch>
          <Route path="/" component={Signin} exact />
          <Route path="/home/:value" component={Home} />

          <Route path="/signup/:value" component={Signup} />
          <Route path="/cleaningdetails/:value" component={CleaningDetails} />
          <Route path="/ConfirmRegistered/:value" component={ConfirmRegistered} />
          <Route path="/ConfirmationGuest/:value" component={ConfirmationGuest} />
          <Route path="/signinstaples" component={SigninStaples} />
          <Route path="/subcategory/:value" component={Subcategories} />
          <Route path="/ordersearch" component={Ordersearch} />
          <Route path="/signinstaples" component={SigninStaples} />
          <Route path="/companysearch/:value" component={CompanySearch} />
          <Route path="/orderidsearch" component={OrderIDSearch} />
          <Route path="/Allorders" component={Allorders} />
          <Route path='/vendorRegistration' component={VendorRegistration}/>
          <Route path='/acCleaning' component={AcCleaning}/>
          <Route path='/acConfirmPage' component={AcConfirmPage}/>
          <Route path='/myOrders' component={MyOrders}/>
          <Route component={PageNotFound} />
        </Switch>
      </HashRouter>
    );
  }
}
