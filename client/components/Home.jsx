import React from 'react';
import {
  Grid,
  Image,
  Icon,
  Divider,
  Segment,
  Label,
  Header,
  Sidebar,
  Menu,
  Dropdown,
  Button,
  Card,
  Input,
} from 'semantic-ui-react';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import Reveal from 'react-reveal';
import AppBar from './AppBarhome.jsx';
import Request from 'superagent';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);
// import { Carousel } from 'react-responsive-carousel';
import { HashRouter, Route, Link } from 'react-router-dom';
import Slider from 'react-slick';

export default class MainLandingPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      category: [],
      search : '',
      searchList: []
    };
  }
  componentWillMount() {
    const context = this;
    // console.log('hiiiiiiiiiiiiiii');
    Request.get('/allCategories')
           .then(function(res) {
             // console.log("list of Categories: ",res.text)

      context.setState({ category: JSON.parse(res.text), searchList: JSON.parse(res.text) });
    });
  }
  handleSearch(){
    // console.log('here');
    console.log("sdaff----->",this.state.search);
    console.log("category ---->", this.state.category);
    var list = this.state.category.filter((item,i)=>{
      if(item.tags.indexOf(this.state.search) != -1 ){
        return item;
      }
    })
    this.setState({
      searchList : list
    })
  }
  render() {
    // console.log('to check Guest value',this.props.match.params.value);
    // console.log('list of', this.state.category);
    const allCategories = this.state.searchList.map((item, key) => {
      if (item.PrimaryCategoryName == 'Office Cleaning') {
        return (
          <Grid.Column as={Link} to={`/subcategory/${this.props.match.params.value}`}>
            <center>
              <Image size="mini" src={item.image_src} />
              <span style={{ color: 'black', fontWeight: 'bold', marginTop: '2%' }}>
                {item.PrimaryCategoryName}
              </span>
            </center>
          </Grid.Column>
        );
      }
      else if(item.PrimaryCategoryName == 'A/C Service'){
        return (
          <Grid.Column as={Link} to='/acCleaning'>
            <center>
              <Image size="mini" src={item.image_src} />
              <span style={{ color: 'black', fontWeight: 'bold', marginTop: '2%' }}>
                {item.PrimaryCategoryName}
              </span>
            </center>
          </Grid.Column>
        );
      }
      else{
        return (
          // <Link to='/subcategory'>
          <Grid.Column>
            <center>
              <Image size="mini" src={item.image_src} />
              <span style={{ fontWeight: 'bold', marginTop: '2%' }}>{item.PrimaryCategoryName}</span>
            </center>
          </Grid.Column>
          // </Link>
        );
      }

    });
    return (
      <div>
        <AppBar />
        <Grid>
          <Grid.Row>
            <Grid.Column width={16}>
              <Segment
                inverted
                style={{ backgroundColor: '#CC0000', height: '82%', marginTop: '-3%' }}
              >
                <h3 style={{ letterSpacing: '2px' }}>Your Location</h3>
                <span>
                  <h4>Hoogoorddreef, Amsterdam</h4>{' '}
                  <Icon
                    size="big"
                    position="right"
                    name="angle down"
                    style={{ marginLeft: '78%', marginTop: '-18%' }}
                  />
                </span>
                <Input
                  placeholder="Search for a service"
                  size="big"
                  style={{padding: 10, marginTop: '-20px' }}
                  value= {this.state.search}
                  onChange = {(e)=>{this.setState({search: e.target.value})}}
                />
                <Button circular icon='search' size ="big" style={{backgroundColor:'#b30000',color:'#fff'}} onClick={this.handleSearch.bind(this)}/>
                <Button circular icon='close' size ="big" style={{backgroundColor:'#b30000',color:'#fff'}} onClick= {()=>{this.setState({searchList:this.state.category,search:''})}}/>
                {/* <Button >
                  <Icon size='small' name='search' />
                </Button> */}
                {/* <Button>
                  <Icon name='close' size='small' />
                </Button> */}
              </Segment>
            </Grid.Column>
          </Grid.Row>

          <Grid.Row style={{ marginTop: '-3%' }}>
            <Grid.Column width={16}>
              <Reveal effect="animated fadeInUp">
                <div
                  className="carosel"
                  style={{
                    postion: 'relative',
                  }}
                >
                  <AutoPlaySwipeableViews>
                    <div>
                      <img
                        height="155px"
                        width="100%"
                        src="https://i0.wp.com/discountjugaad.com/wp-content/uploads/2017/09/home_repair_web_home_page_banner.jpg?resize=866%2C217&ssl=1"
                      />
                    </div>
                    <div>
                      <img
                        height="155px"
                        width="100%"
                        src="http://d2vj71og9gdu4k.cloudfront.net/WEB/banners/20170831-120513-Freedom-30-Home-page-banner.jpg"
                      />
                    </div>
                  </AutoPlaySwipeableViews>
                </div>
              </Reveal>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row style={{ marginTop: '3%' }}>
            {/* <Grid.Column width={2}/> */}
            <Grid.Column width={16}>
              <center>
                <h3>RECOMMENDED SERVICES</h3>
              </center>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Grid celled>
          <Grid.Row columns={3} stretched>
            {/* <Grid.Column width={1}/> */}
            {allCategories}
            {/* <Grid.Column width={1}/> */}
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}
