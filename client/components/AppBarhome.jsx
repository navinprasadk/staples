import React, { Component } from 'react';
import { Grid, Menu, Icon, Label, Header, Image } from 'semantic-ui-react';
import '../styles/style.css';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';

// import { connect } from "react-redux";
import { Link } from 'react-router-dom';

export default class AppBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  handleToggle() {
    this.setState({
      open: !this.state.open,
    });
  }
  handleClose() {
    this.setState({ open: false });
  }
  render() {
    return (
      <div style={{ overflow: 'hidden' }}>
        <Grid>
          <Grid.Row only="mobile" style={{ paddingBottom: '5%' }}>
            <Grid.Column width={16}>
              <Menu secondary style={{ backgroundColor: 'white', color: '#FFFFFF' }}>
                <Menu.Item style={{ color: 'black' }}>
                  <Icon size="large" name="bars" onClick={this.handleToggle.bind(this)} />
                </Menu.Item>
                <Menu.Item style={{ color: 'white', fontSize: '24px' }}>
                  <center>
                    <Image
                      style={{ marginLeft: '-10%' }}
                      size="medium"
                      //src="http://logos-download.com/wp-content/uploads/2016/12/Staples_logo_logotype.png"
                      src="./client/Images/logo.png"
                    />
                  </center>
                </Menu.Item>
                {/* <Menu.Item header id="titleText" style={{fontWeight:'normal', letterSpacing:'3px', textTransform:'capitalize', color:"#FFFFFF", float:"left", fontSize:'140%'}} name='Aquaberry' /> */}
                {/* <Menu.Menu position='right'>
                    <Link to='/wishlist'>
                    <Menu.Item name="Signin" style={{marginTop:'15%'}}><Icon size="large" name="empty heart" style={{color:'#FFFFFF'}} />
                    <Label color="red" circular style={{position: 'absolute',zIndex: 100, marginTop: '-23%',marginLeft: '25%'}}>{this.props.cartDetails.count}</Label>
                  </Menu.Item>
                  </Link>
                    <Menu.Item name="Signin"><Icon size="large" name="shopping cart" style={{color:'#FFFFFF'}} />
                    <Label color="red" circular style={{position: 'absolute',zIndex: 100, marginTop: '-23%',marginLeft: '25%'}}>{this.props.cartDetails.cartCount}</Label>
</Menu.Item>
                  </Menu.Menu> */}
              </Menu>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Drawer
          docked={false}
          width={250}
          open={this.state.open}
          onRequestChange={open => this.setState({ open })}
        >
          <Menu.Item style={{ marginTop: '5%' }}>
            <center>
              <Image avatar circular size="tiny" src={this.state.profile} />
            </center>
            <center />
          </Menu.Item>
          <Menu.Item />
          <Menu.Item />
          <Link to="/home/$">
            <MenuItem style={{ marginTop: '10%' }}>
              <Header as="h4" style={{ letterSpacing: '2px', textTransform: 'capitalize' }}>
                <Icon size="small" name="home" style={{ color: '#212121' }} />Home
              </Header>
            </MenuItem>
          </Link>
          {/* <MenuItem>
          <Header as='h4' style={{letterSpacing:"2px", textTransform:"capitalize"}}><Icon size='small' name='shopping bag' style={{color:'#212121'}}/>Bag</Header>
        </MenuItem> */}


          <Link to="/myOrders">
            <MenuItem>
              <Header
                as="h4"
                style={{ letterSpacing: "2px", textTransform: "capitalize" }}
              >
                <Icon size="small" name="ticket" style={{ color: "#212121" }} />My
                Orders
              </Header>
            </MenuItem>
          </Link>

          {/* <Link to="/order"> */}
          <MenuItem style={{ marginTop: '0%' }}>
            <Header as="h4" style={{ letterSpacing: '2px', textTransform: 'capitalize' }}>
              <Icon size="small" name="user circle outline" style={{ color: '#212121' }} />My
              Account
            </Header>
          </MenuItem>
          {/* </Link> */}

          <Link to="/signinstaples">
            <MenuItem>
              <Header as="h4" style={{ letterSpacing: '2px', textTransform: 'capitalize' }}>
                <Icon size="small" name="sign in" style={{ color: '#212121' }} />Service Channel Admin Login
              </Header>
            </MenuItem>
          </Link>

          <Link to="/vendorRegistration">
            <MenuItem>
              <Header as="h4" style={{ letterSpacing: '2px', textTransform: 'capitalize' }}>
                <Icon size="small" name="sign in" style={{ color: '#212121' }} />Register as Vendor
              </Header>
            </MenuItem>
          </Link>

          <MenuItem>
            <Header as="h4" style={{ letterSpacing: '2px', textTransform: 'capitalize' }}>
              <Icon size="small" name="setting" style={{ color: '#212121' }} />Settings
            </Header>
          </MenuItem>
        </Drawer>
      </div>
    );
  }
}
// function mapStateToProps(state) {
//   return {
//     cartDetails: state.cartReducer
//   };
// }

// export default connect(mapStateToProps, null)(AppBar);
