import React, { Component } from 'react';
import { Grid, Input, Button, Icon, Divider, Image, Dropdown } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

export default class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const options = [
      {
        key: 'nl',
        value: 'nl',
        flag: 'nl',
        text: '+31',
      },
      // { key: 'gb', value: 'gb', flag: 'gb', text: '+44' }
    ];
    console.log('mobile number in signuo', this.props.match.params.value);
    return (
      <div>
        <Grid>
          {/* <Grid.Row style={{marginTop:'15%'}}>
          <Grid.Column width={1}/>
          <Grid.Column width={2}>

            <Button onClick={()=>{Window.open('/')}} as={Link} to='/'><Icon Link to='/'  size='large' name='arrow left'/></Button>


          </Grid.Column>
        </Grid.Row> */}
          <Grid.Row only="mobile" style={{ marginTop: '8%' }}>
            <Grid.Column width={2} />
            <Grid.Column
              width={12}
              style={{
                textAlign: 'center',
                fontWeight: 'bold',
                letterSpacing: '3px',
              }}
            >
              <center>
                <Image
                  size="medium"
                  src="./client/Images/logo.png"
                    //src="http://logos-download.com/wp-content/uploads/2016/12/Staples_logo_logotype.png"
                />
              </center>
              {/* <h1 style={{fontSize:'21px',fontFamily:'Source Sans Pro sans serif'}}>Signup in Staples</h1> */}
            </Grid.Column>
            <Grid.Column width={2} />
          </Grid.Row>
          <Grid.Row style={{ marginTop: '8%' }}>
            <Grid.Column width={2} />
            <Grid.Column width={12}>
              <Input
                fluid
                type="number"
                label={<Dropdown defaultValue={options[0].value} options={options} />}
                labelPosition="left"
                placeholder={this.props.match.params.value}
              />
            </Grid.Column>
            <Grid.Column width={2} />
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={2} />
            <Grid.Column width={12}>
              <Input fluid color="black" placeholder="Email Address" />
            </Grid.Column>
            <Grid.Column width={2} />
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={2} />
            <Grid.Column width={12}>
              <Input fluid placeholder="Full Name" />
            </Grid.Column>
            <Grid.Column width={2} />
          </Grid.Row>

          <Grid.Row>
            <Grid.Column width={2} />
            <Grid.Column width={12}>
              <Link to={`/home/${this.props.match.params.value}`}>
                <Button
                  // as={Link} to={`/signup/${this.state.mobile}`}
                  fluid
                  style={{
                    borderRadius: '5px',
                    backgroundColor: '#cc0000',
                    letterSpacing: '2px',
                    fontWeight: 'bold',
                    textTransform: 'uppercase',
                    fontSize: '100%',
                    color: '#FAFAFA',
                  }}
                >
                  NEXT
                  <Icon name="arrow right" />
                </Button>
              </Link>
            </Grid.Column>
            <Grid.Column width={2} />
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={2} />
            <Grid.Column width={12}>
              <p style={{ fontSize: '11px', fontFamily: 'Source Sans Pro sans serif' }}>
                By signing up, you agree to our terms of use and privacy policy
              </p>
            </Grid.Column>
            <Grid.Column width={2} />
          </Grid.Row>
          <Grid.Row style={{ marginTop: '-2%' }} only="mobile">
            <Grid.Column width={1} />
            <Grid.Column
              width={14}
              style={{
                textAlign: 'center',
                textTransform: 'capitalize',
                letterSpacing: '2px',
                fontFamily: 'Raleway',
              }}
            >
              <Divider horizontal style={{ color: 'black' }}>
                Or
              </Divider>
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={6} />
            <Grid.Column width={6}>
              <h4>Continue with</h4>
            </Grid.Column>
            <Grid.Column width={6} />
          </Grid.Row>
          <Grid.Row style={{ marginTop: '-2%' }} only="mobile">
            <Grid.Column width={3} />
            <Grid.Column width={10}>
              <Link to="/fb">
                <Button
                  fluid
                  color="facebook"
                  style={{ letterSpacing: '2px', borderRadius: '25px' }}
                >
                  <Icon name="facebook" style={{ float: 'left' }} />FACEBOOK
                </Button>
              </Link>
            </Grid.Column>
            <Grid.Column width={3} />
          </Grid.Row>
          <Grid.Row style={{ marginTop: '-3%' }} only="mobile">
            <Grid.Column width={3} />
            <Grid.Column width={10}>
              <Button
                fluid
                color="google plus"
                style={{ letterSpacing: '2px', borderRadius: '25px' }}
              >
                <Icon name="google" style={{ float: 'left' }} />GOOGLE
              </Button>
            </Grid.Column>
            <Grid.Column width={3} />
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}
