import React, { Component } from 'react';
import {
  Grid,
  Input,
  Button,
  Icon,
  Divider,
  Image,
  Dropdown,
  Form,
  Radio,
} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import AppBar from './AppBarhome.jsx';
import Allorders from './AllOrders.jsx';

export default class OrderSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      compantid: '',
      orderid: '',
    };
  }
  handleChange = (e, { value }) => this.setState({ value });
  getCompanyid(event, e) {
    console.log('value', e.value);
    this.setState({ compantid: e.value });
  }
  getorderid(event, e) {
    this.setState({ orderid: e.value });
  }
  render() {
    return (
      <div>
        <AppBar />
        <Grid>
          <Grid.Row style={{marginTop:'20%'}}>
            <Grid.Column width={3}/>
            <Grid.Column width={10}>
              {/* <center> */}
              <Form>
                <Form.Field>
                  <Radio
                    label="Search by Company ID"
                    name="radioGroup"
                    value="Search by Company ID"
                    checked={this.state.value === 'Search by Company ID'}
                    onChange={this.handleChange}
                  />
                </Form.Field>
                <Form.Field>
                  <Radio
                    label="Search by Order ID"
                    name="radioGroup"
                    value="Search by Order ID"
                    checked={this.state.value === 'Search by Order ID'}
                    onChange={this.handleChange}
                  />
                </Form.Field>
                <Link to="/Allorders">
                  <Form.Field>
                    <Radio
                      label="All Orders"
                      name="radioGroup"
                      value="All Orders"
                      checked={this.state.value === 'All Orders'}
                      onChange={this.handleChange}
                    />
                  </Form.Field>
                </Link>
              </Form>
              {/* </center> */}
            </Grid.Column>
            <Grid.Column width={3} />
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={2} />
            <Grid.Column width={10}>
              {this.state.value == 'Search by Company ID' ? <h3>Enter Company ID</h3> : null}
              {this.state.value == 'Search by Order ID' ? <h3>Enter Order ID</h3> : null}
            </Grid.Column>
            <Grid.Column width={4} />
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={2} />
            <Grid.Column width={8}>
              {this.state.value == 'Search by Company ID' ? (
                <Input placeholder="Search by Company ID" onChange={this.getCompanyid.bind(this)} />
              ) : null}
              {this.state.value == 'Search by Order ID' ? (
                <Input placeholder="Search by Order ID" onChange={this.getorderid.bind(this)} />
              ) : null}
            </Grid.Column>
            <Grid.Column width={3}>
              {this.state.value == 'Search by Company ID' ? (
                <Button
                  as={Link}
                  to={`/companysearch/${this.state.compantid}`}
                  style={{ backgroundColor: '#cc0000' }}
                >
                  Go
                </Button>
              ) : null}
              {this.state.value == 'Search by Order ID' ? (
                <Button as={Link} to="/orderidsearch" style={{ backgroundColor: '#cc0000' }}>
                  Go
                </Button>
              ) : null}
            </Grid.Column>
            <Grid.Column width={3} />
          </Grid.Row>
        </Grid>
        {/* {this.state.value == 'All Orders' ? <Allorders/> : null} */}
      </div>
    );
  }
}
