import React from 'react';
import { Menu, Grid, Icon } from 'semantic-ui-react';

const AppBar = ({ textValue }) => (
  <Menu inverted widths={16}>
    {/* <Menu.Item><Icon name='arrow-left'/></Menu.Item> */}
    <Menu.Item
      style={{
        fontSize: '110%',
        textTransform: 'uppercase',
        fontWeight: '800',
        letterSpacing: '2px',
        fontFamily: 'Open Sans',
      }}
      name={textValue}
    />
  </Menu>
);
export default AppBar;
