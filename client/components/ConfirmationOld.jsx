import React, { Component } from 'react';
import { Grid, Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import AppBar from './AppBar.jsx';

// const Confirmation = () => {
//   const {
//     cleaningType,
//     numberOfFloors,
//     numberOfRooms,
//     numberOfBathroom,
//     area,
//   } = this.props.location.state;
//   return (
//     <div className="container">
//       <AppBar />
//       <Grid>
//         <Grid.Row only="mobile">
//           <Grid.Column width={1} />
//           <Grid.Column width={6}>Type</Grid.Column>
//           <Grid.Column width={8}>{cleaningType}</Grid.Column>
//           <Grid.Column width={1} />
//         </Grid.Row>

//         <Grid.Row only="mobile">
//           <Grid.Column width={1} />
//           <Grid.Column width={6}>Floors</Grid.Column>
//           <Grid.Column width={8}>{numberOfFloors}</Grid.Column>
//           <Grid.Column width={1} />
//         </Grid.Row>

//         <Grid.Row only="mobile">
//           <Grid.Column width={1} />
//           <Grid.Column width={6}>Rooms</Grid.Column>
//           <Grid.Column width={8}>{numberOfRooms}</Grid.Column>
//           <Grid.Column width={1} />
//         </Grid.Row>

//         <Grid.Row only="mobile">
//           <Grid.Column width={1} />
//           <Grid.Column width={6}>Bathroom</Grid.Column>
//           <Grid.Column width={8}>{numberOfBathroom}</Grid.Column>
//           <Grid.Column width={1} />
//         </Grid.Row>

//         <Grid.Row only="mobile">
//           <Grid.Column width={1} />
//           <Grid.Column width={6}>Area in sq.ft</Grid.Column>
//           <Grid.Column width={8}>{area}</Grid.Column>
//           <Grid.Column width={1} />
//         </Grid.Row>

//         <Grid.Row only="mobile">
//           <Grid.Column width={1} />
//           <Grid.Column width={6}>Select the Date</Grid.Column>
//           <Grid.Column width={8} />
//           <Grid.Column width={1} />
//         </Grid.Row>

//         <Grid.Row only="mobile">
//           <Grid.Column width={1} />
//           <Grid.Column width={6}>Address</Grid.Column>
//           <Grid.Column width={8}>{}</Grid.Column>
//           <Grid.Column width={1} />
//         </Grid.Row>

//         <Grid.Row only="mobile">
//           <Grid.Column width={2} />

//           <Grid.Column width={12}>
//             <center>
//               <Button
//                 style={{
//                   backgroundColor: '#000',
//                   color: '#fff',
//                   letterSpacing: '2px',
//                 }}
//               >
//                 <Link to="/serviceConfirmation"> Confirm</Link>
//               </Button>
//             </center>
//           </Grid.Column>
//           <Grid.Column width={2} />
//         </Grid.Row>
//       </Grid>
//     </div>
//   );
// };
// // Confirmation.propTypes = {
// //   cleaningType: PropTypes.object,
// //   numberOfFloors: PropTypes.object,
// //   numberOfRooms: PropTypes.object,
// //   numberOfBathroom: PropTypes.object,
// //   area: PropTypes.object,
// //   date: PropTypes.object,
// //   address: PropTypes.object,
// // };

// export default Confirmation;

export default class Confirmation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      a: null,
    };
  }

  render() {
    const {
      cleaningType,
      numberOfFloors,
      numberOfRooms,
      numberOfBathroom,
      area,
      date,
      address,
    } = this.props.location.state;
    return (
      <div className="container">
        <AppBar />
        <Grid>
          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6}>Type</Grid.Column>
            <Grid.Column width={8}>{cleaningType}</Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6}>Floors</Grid.Column>
            <Grid.Column width={8}>{numberOfFloors}</Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6}>Rooms</Grid.Column>
            <Grid.Column width={8}>{numberOfRooms}</Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6}>Bathroom</Grid.Column>
            <Grid.Column width={8}>{numberOfBathroom}</Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6}>Area in sq.ft</Grid.Column>
            <Grid.Column width={8}>{area}</Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6}>Select the Date</Grid.Column>
            <Grid.Column width={8}>{date}</Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6}>Address</Grid.Column>
            <Grid.Column width={8}>{address}</Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={2} />

            <Grid.Column width={12}>
              <center>
                <Button
                  style={{
                    backgroundColor: '#000',
                    color: '#fff',
                    letterSpacing: '2px',
                  }}
                >
                  <Link to="/OrderMessage"> Confirm</Link>
                </Button>
              </center>
            </Grid.Column>
            <Grid.Column width={2} />
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}
Confirmation.propTypes = {
  cleaningType: PropTypes.object,
  numberOfFloors: PropTypes.object,
  numberOfRooms: PropTypes.object,
  numberOfBathroom: PropTypes.object,
  area: PropTypes.object,
  date: PropTypes.object,
  address: PropTypes.object,
};
