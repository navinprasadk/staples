import React from 'react';
import { Link } from 'react-router-dom';
import { Image, List, Header, Icon, Grid } from 'semantic-ui-react';
import Appbar from './AppBarhome.jsx';

export default class Subcategories extends React.Component{
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render(){
    return(
      <div>
        <Appbar />
        <Grid>
          <Grid.Row>
            <Grid.Column width={16}>
              <center>
                <h3 style={{ letterSpacing: '2px' }}>Sub-Category</h3>
              </center>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            {/* <Grid.Column width={1}/> */}
            <Grid.Column width={15} style={{ marginLeft: '4%' }}>
              <List relaxed="very" divided verticalAlign="middle">
                <List.Item>
                  <Image src="./client/Images/plumber.png" size="mini" circular />
                  <List.Content style={{ fontSize: '17px' }}>
                    <List.Header as="a">Carpet Cleaning </List.Header>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <Image src="./client/Images/electrical1.png" size="mini" circular />
                  <List.Content style={{ fontSize: '17px' }}>
                    <List.Header as="a">Window Cleaning</List.Header>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <Image src="./client/Images/carpenter.png" size="mini" circular />
                  <List.Content style={{ fontSize: '17px' }}>
                    <List.Header as="a">Pantry Cleaning</List.Header>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <Image src="./client/Images/homeCleaning.png" size="mini" circular />
                  <List.Content style={{ fontSize: '17px' }}>
                    <List.Header as="a">
                      Bathroom Cleaning
                    </List.Header>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <Image src="./client/Images/bathroom.png" size="mini" circular />
                  <List.Content style={{ fontSize: '17px' }}>
                    <List.Header as="a">
                      <Link to={`/cleaningdetails/${this.props.match.params.value}`}>Complete Cleaning</Link></List.Header>
                  </List.Content>
                </List.Item>
              </List>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    )
  }

}
