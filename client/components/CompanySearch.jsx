import React, { Component } from 'react';
import { Grid, Input, Button, Icon, Menu, Form, Dropdown,List, Segment, Divider } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import SwipeableViews from 'react-swipeable-views';
import AppBar from './AppBarhome.jsx';
import Request from 'superagent';

export default class CompanySearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index:'',
      activeItem:'Submitted',
      searchbycompany:[]
    };
  }
  componentWillMount(){
    var context = this;
  Request.get('/allworkorders')
              .then(function(res) {
                console.log("All work orders:",res.text)
                context.setState({searchbycompany: JSON.parse(res.text)})
              });
}
  handleItemClick(e,{name}){
    if(name=='Submitted')
    this.setState({index:0,activeItem:name})

  else if(name=='In Progress')
  this.setState({index:1,activeItem:name})

else
this.setState({index:2,activeItem:name})
}

  render() {
    console.log('all details',this.state.searchbycompany);
    var companyid = this.state.searchbycompany.map((item) =>{
      if(item.CompanyId == 'CM100023'){
        if(item.Status == 'Submitted'){
          return (
            <Grid.Row style={{borderRadius:'40px',backgroundColor:'#DFE5E3',marginBottom:'2%',marginLeft:'6%',marginRight:'6%'}}>
              <Grid.Column width={5}>
                <center><h5><Link to='/orderidsearch'>{item.OrderId}</Link></h5></center>
              </Grid.Column>
              <Grid.Column width={5}>
                <center>
                  <h5>{item.Submitted_date}</h5>
                </center>
              </Grid.Column>
              <Grid.Column width={5}>
                <center>
                  <h5>{item.Assignee}</h5>
                </center>
              </Grid.Column>
            </Grid.Row>
          )
        }
      }
    })
    var companyid1 = this.state.searchbycompany.map((item) =>{
      if(item.CompanyId == 'CM100023'){
        if(item.Status == 'Completed'){
          return (
            <Grid.Row style={{borderRadius:'40px',backgroundColor:'#DFE5E3',marginBottom:'2%',marginLeft:'6%',marginRight:'6%'}}>
              <Grid.Column width={5}>
                <center><h5><Link to='/orderidsearch'>{item.OrderId}</Link></h5></center>
              </Grid.Column>
              <Grid.Column width={5}>
                <center>
                  <h5>{item.Submitted_date}</h5>
                </center>
              </Grid.Column>
              <Grid.Column width={5}>
                <center>
                  <h5>{item.Assignee}</h5>
                </center>
              </Grid.Column>
            </Grid.Row>
          )
        }
      }
    })
    var companyid2 = this.state.searchbycompany.map((item) =>{
      if(item.CompanyId == 'CM100023'){
        if(item.Status == 'Inprogress'){
          return (
            <Grid.Row style={{borderRadius:'40px',backgroundColor:'#DFE5E3',marginBottom:'2%',marginLeft:'6%',marginRight:'6%'}}>
              <Grid.Column width={5}>
                <center><h5><Link to='/orderidsearch'>{item.OrderId}</Link></h5></center>
              </Grid.Column>
              <Grid.Column width={5}>
                <center>
                  <h5>{item.Submitted_date}</h5>
                </center>
              </Grid.Column>
              <Grid.Column width={5}>
                <center>
                  <h5>{item.Assignee}</h5>
                </center>
              </Grid.Column>
            </Grid.Row>
          )
        }
      }
    })
    const { activeItem } = this.state;
    return (
      <div>
        <AppBar/>

          {this.state.searchbycompany.map((item,key) =>{
            if(key == 0){
            return(
              <Grid style={{marginTop:"3%"}} key={key}>
              <Grid.Row>
                <Grid.Column width={2}/>
                <Grid.Column width={6}>
                  <h4 style={{color:'#cc0000'}}>Company ID</h4>
                </Grid.Column>
                <Grid.Column width={1}>
                  <h4>:</h4>
                </Grid.Column>
                <Grid.Column width={6}>
                  <h4>{item.CompanyId}</h4>
                </Grid.Column>
                <Grid.Column width={1}/>
              </Grid.Row>
              <Grid.Row style={{marginTop:'-3%'}}>
                <Grid.Column width={2}/>
                <Grid.Column width={6}>
                  <h4 style={{color:'#cc0000'}}>Company Name</h4>
                </Grid.Column>
                <Grid.Column width={1}>
                  <h4>:</h4>
                </Grid.Column>
                <Grid.Column width={6}>
                  <h4>{item.CompanyName}</h4>
                </Grid.Column>
                <Grid.Column width={1}/>
              </Grid.Row>
              <Grid.Row style={{marginTop:'-3%'}}>
                <Grid.Column width={2}/>
                <Grid.Column width={6}>
                  <h4 style={{color:'#cc0000'}}>Company Address</h4>
                </Grid.Column>
                <Grid.Column width={1}>
                  <h4>:</h4>
                </Grid.Column>
                <Grid.Column width={6}>
                  <h4>{item.CompanyAddress}</h4>
                </Grid.Column>
                <Grid.Column width={1}/>
              </Grid.Row>
              <Grid.Row style={{marginTop:'-3%'}}>
                <Grid.Column width={2}/>
                <Grid.Column width={6}>
                  <h4 style={{color:'#cc0000'}}>Company Type</h4>
                </Grid.Column>
                <Grid.Column width={1}>
                  <h4>:</h4>
                </Grid.Column>
                <Grid.Column width={6}>
                  <h4>IT</h4>
                </Grid.Column>
                <Grid.Column width={1}/>
              </Grid.Row>
              </Grid>
            )}
          })}

        {/* <Grid.Row>
          <Grid.Column width={2}/>
          {this.state.searchbycompany.map((item,key) => {
            if(key == 0)
            {
              return(
                <Grid.Column width={12} key={key}>
                    <h4>Company ID : {item.CompanyId}</h4>
                    <h4 style={{ fontFamily: 'Open Sans',marginTop:'-3%'}}>Company Name : {item.CompanyName}</h4>
                    <h4 style={{ fontFamily: 'Open Sans',marginTop:'-3%'}}>Company Address : {item.CompanyAddress}</h4>
                    <h4 style={{ fontFamily: 'Open Sans', marginTop:'-3%'}}>Company Type : IT</h4>
                </Grid.Column>
              )
            }
          })}

          <Grid.Column width={2}/>
        </Grid.Row> */}

        <Grid>
          <Grid.Row/>
        <Grid.Row>
          <Grid.Column width={1}/>
          <Grid.Column width={14}>
            <Menu pointing secondary style={{color:'#cc0000',marginTop:'-5%'}}>
          <Menu.Item name='Submitted' active={activeItem === 'Submitted'} onClick={this.handleItemClick.bind(this)} />
          <Menu.Item name='In Progress' active={activeItem === 'In Progress'} onClick={this.handleItemClick.bind(this)} />
          <Menu.Item name='Completed' active={activeItem === 'Completed'} onClick={this.handleItemClick.bind(this)} />
        </Menu>
        <SwipeableViews index={this.state.index} disabled>
    <div style={{height:'50vh'}}>
      <Grid >
          <Grid.Row style={{borderRadius:'40px',marginTop:'5%',marginLeft:'6%',marginRight:'6%'}}>
            {/* <Grid.Column width={1}/> */}
            <Grid.Column width={5}>
              <center>
                <h4 style={{fontWeight:'bold'}}>Order Id</h4>
              </center>
            </Grid.Column>
            <Grid.Column width={5}>
              <center>
                <h4>Submitted Date</h4>
              </center>
            </Grid.Column>
            <Grid.Column width={5}>
              <center>
                <h4>Assigned</h4>
              </center>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={1}/>
            <Grid.Column width={13}>
              <Divider style={{backgroundColor:'black',marginTop:'-6%'}}/>
            </Grid.Column>
            <Grid.Column width={2}/>
          </Grid.Row>
          {companyid}
      </Grid>

    </div>
    <div style={{height:'50vh'}}>
      <Grid >
          <Grid.Row style={{borderRadius:'40px',marginTop:'5%',marginLeft:'6%',marginRight:'6%'}}>
            {/* <Grid.Column width={1}/> */}
            <Grid.Column width={5}>
              <center>
                <h4 style={{fontWeight:'bold'}}>Order Id</h4>
              </center>
            </Grid.Column>
            <Grid.Column width={5}>
              <center>
                <h4>Submitted Date</h4>
              </center>
            </Grid.Column>
            <Grid.Column width={5}>
              <center>
                <h4>Assigned</h4>
              </center>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={1}/>
            <Grid.Column width={13}>
              <Divider style={{backgroundColor:'black',marginTop:'-6%'}}/>
            </Grid.Column>
            <Grid.Column width={2}/>
          </Grid.Row>
          {companyid2}
      </Grid>

    </div>

    <div>
      <div style={{height:'50vh'}}>
        <Grid >
            <Grid.Row style={{borderRadius:'40px',marginTop:'5%',marginLeft:'6%',marginRight:'6%'}}>
              {/* <Grid.Column width={1}/> */}
              <Grid.Column width={5}>
                <center>
                  <h4 style={{fontWeight:'bold'}}>Order Id</h4>
                </center>
              </Grid.Column>
              <Grid.Column width={5}>
                <center>
                  <h4>Submitted Date</h4>
                </center>
              </Grid.Column>
              <Grid.Column width={5}>
                <center>
                  <h4>Assigned</h4>
                </center>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width={1}/>
              <Grid.Column width={13}>
                <Divider style={{backgroundColor:'black',marginTop:'-6%'}}/>
              </Grid.Column>
              <Grid.Column width={2}/>
            </Grid.Row>
            {companyid1}
        </Grid>

      </div>
    </div>

  </SwipeableViews>
        </Grid.Column>
        <Grid.Column width={1}/>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <center>
              <Button as={Link} to='/ordersearch'>Back</Button>
            </center>
          </Grid.Column>
        </Grid.Row>
</Grid>
      </div>
    );
  }
}
