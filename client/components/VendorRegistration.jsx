// import React, { Component } from 'react';
// import {
//   Radio,
//   Header,
//   Grid,
//   Icon,
//   List,
//   Form,
//   Button,
//   Input,
//   Label,
//   Modal,
// } from 'semantic-ui-react';
// import FloatingActionButton from 'material-ui/FloatingActionButton';
// import ContentAdd from 'material-ui/svg-icons/content/add';
// import { Link } from 'react-router-dom';
// import request from 'superagent';
//
// import AppBar from './AppBarhome.jsx';
// import CategoryForVendorRegistration from './CategoryForVendorRegistration.jsx';
//
// // Internal styling
// const containerStyle = {
//   backgroundImage: "url('./client/Images/bg-red.jpg')",
//   backgroundAttachment: 'fixed',
//   overflow: 'hidden',
// };
// const titleStyle = {
//   textAlign: 'center',
//   fontWeight: '600',
//   letterSpacing: '1px',
//   fontFamily: 'Open Sans',
//   color: 'white',
//   fontSize: 20,
//   textTransform: 'capitalise',
// };
// const labelStyle = {
//   textTransform: 'capitalise',
//   letterSpacing: '2px',
//   textAlign: 'center',
//   fontWeight: '600',
//   fontSize: 14,
//   fontFamily: 'Open Sans',
//   color: 'white',
//   backgroundColor: '#CC0000',
// };
// const inputLabelStyle = {
//   fontFamily: 'Open Sans',
//   textTransform: 'capitalise',
//   fontSize: 14,
//   color: 'white',
//   fontWeight: '600',
// };
// const submitButtonStyle = {
//   letterSpacing: '2px',
//   boxShadow: '0 6px 10px 0 hsla(0, 0%, 0%, 0.2)',
//   fontFamily: 'Open Sans',
//   backgroundColor: '#CC0000',
//   color: 'white',
//   padding: '12px',
// };
//
// export default class VendorRegistration extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       vendorName: '',
//       vendorLocation: '',
//       vendorMobileNumber: 0,
//       rating: 0,
//       firstDocumentModal: false,
//       firstDocumentUploading: false,
//       firstDocumentUploaded: false,
//       secondDocumentUploading: false,
//       secondDocumentUploaded: false,
//       openModal: false,
//       categoryDisplayCount: 1,
//       selectedCategories: [
//         {
//           category: 'Full Cleaning',
//           Bathroom: 0,
//           Pantry: 0,
//           Room: 0,
//           Window: 0,
//           AdditionalSqFt: 0,
//         },
//       ],
//     };
//     this.handleNameChange = this.handleNameChange.bind(this);
//     this.handleLocationChange = this.handleLocationChange.bind(this);
//     this.handleMobileNumberChange = this.handleMobileNumberChange.bind(this);
//     this.handleRadioChange = this.handleRadioChange.bind(this);
//     this.handleCategoryChange = this.handleCategoryChange.bind(this);
//     this.handleUploadFirstDocument = this.handleUploadFirstDocument.bind(this);
//     this.handleUploadSecondDocument = this.handleUploadSecondDocument.bind(this);
//     this.handleFAB = this.handleFAB.bind(this);
//     this.handleSubmit = this.handleSubmit.bind(this);
//   }
//   // set the vendor name in state
//   handleNameChange = (e) => {
//     this.setState({ vendorName: e.target.value });
//   };
//   // set the vendor location in state
//   handleLocationChange = (e) => {
//     this.setState({ vendorLocation: e.target.value });
//   };
//   // set the vendor location in state
//   handleMobileNumberChange = (e) => {
//     this.setState({ vendorMobileNumber: e.target.value });
//   };
//   handleCategoryChange(value, index) {
//     const { selectedCategories } = this.state;
//     if (!selectedCategories.find(cat => value == cat.category)) {
//       if (value === 'Full Cleaning') {
//         selectedCategories.splice(index, 1, {
//           category: value,
//           Bathroom: 0,
//           Pantry: 0,
//           Room: 0,
//           Window: 0,
//           AdditionalSqFt: 0,
//         });
//       } else if (value === 'Deep Cleaning') {
//         selectedCategories.splice(index, 1, {
//           category: value,
//           Floor: 0,
//           Window: 0,
//           Door: 0,
//           Bathroom: 0,
//           Pantry: 0,
//           Room: 0,
//           AdditionalSqFt: 0,
//         });
//       } else if (value === 'AC Maintenance') {
//         selectedCategories.splice(index, 1, {
//           category: value,
//           AreaInSqFt: 0,
//         });
//       }
//     } else {
//       alert('Category already exists. Select a different category');
//     }
//
//     this.setState(
//       {
//         selectedCategories,
//       },
//       () => {
//         console.log(selectedCategories);
//       },
//     );
//   }
//
//   handleValueChange(value, index, fieldName) {
//     const { selectedCategories } = this.state;
//     const category = selectedCategories[index];
//     category[fieldName] = value;
//     selectedCategories.splice(index, 1, category);
//     this.setState({ selectedCategories }, () => {
//       console.log('handleValueChange:', selectedCategories);
//     });
//   }
//
//   // For handling the radio button in the form
//   handleRadioChange = (e, { value }) => this.setState({ value });
//
//   // Below function passes the form data from client to server side
//   handleSubmit = () => {
//     // Generate the random values between 3.5 and 5.0
//     const context = this;
//     this.setState({
//       openModal: true,
//     });
//
//     request
//       .post('/vendorRequest')
//       .query({
//         vendor_name: this.state.vendorName,
//         vendor_location: this.state.vendorLocation,
//         vendor_mobilenumber: this.state.vendorMobileNumber,
//         vendor_category: JSON.stringify(this.state.selectedCategories),
//         vendor_distance: this.state.value,
//       })
//       .end((err, res) => {
//         console.log(res, 'response from db');
//       });
//       // console.log(this.state.selectedCategories,"In client side");
//   };
//
//   // Uploading the first document in the form
//   handleUploadFirstDocument = () => {
//     this.setState({ firstDocumentUploading: true }, () => {
//       const timerId = setInterval(() => {
//         this.setState(
//           {
//             firstDocumentUploading: false,
//             firstDocumentUploaded: true,
//           },
//           () => {
//             clearInterval(timerId);
//           },
//         );
//       }, 2000);
//     });
//   };
//
//   // Uploading the second document in the form
//   handleUploadSecondDocument = () => {
//     this.setState({ secondDocumentUploading: true }, () => {
//       const timerId = setInterval(() => {
//         this.setState(
//           {
//             secondDocumentUploading: false,
//             secondDocumentUploaded: true,
//           },
//           () => {
//             clearInterval(timerId);
//           },
//         );
//       }, 2000);
//     });
//   };
//
//   // Floating Action Button -> used to add more category
//   handleFAB = () => {
//     let { categoryDisplayCount, selectedCategories } = this.state;
//     if (categoryDisplayCount < 3) {
//       categoryDisplayCount++;
//     }
//     selectedCategories.push({
//       category: 'Full Cleaning',
//       Bathroom: '',
//       Pantry: '',
//       Room: '',
//       Window: '',
//       AdditionalSqFt: '',
//     });
//     this.setState(
//       {
//         categoryDisplayCount,
//         selectedCategories,
//       },
//       () => {
//         console.log(selectedCategories);
//       },
//     );
//   };
//
//   render() {
//     const displayCategories = [];
//     for (let i = 0; i < this.state.categoryDisplayCount; i++) {
//       displayCategories.push(<CategoryForVendorRegistration
//         index={i}
//         handleCategoryChange={value => this.handleCategoryChange(value, i)}
//         handleValueChange={(value, index, fieldName) =>
//             this.handleValueChange(value, index, fieldName)
//           }
//         selectedCategories={this.state.selectedCategories}
//       />);
//     }
//     // console.log(this.state.vendorName, 'vendor name');
//     // console.log(this.state.vendorLocation, 'vendor location');
//     // console.log(this.state.rating, 'rating');
//     // console.log(this.state.selectedCategories, 'category in frontend');
//     return (
//       <div style={containerStyle}>
//         <AppBar />
//         <Grid>
//           <Grid.Row only="mobile">
//             <Grid.Column width={2} />
//             <Grid.Column width={12}>
//               <p style={titleStyle}>Vendor Registration Form</p>
//             </Grid.Column>
//             <Grid.Column width={2} />
//           </Grid.Row>
//
//           {/* Label for details */}
//           <Grid.Row only="mobile">
//             <Grid.Column width={8}>
//               <Label tag style={labelStyle}>
//                 Details
//               </Label>
//             </Grid.Column>
//             <Grid.Column width={8} />
//           </Grid.Row>
//
//           {/* Vendor Name */}
//           <Grid.Row only="mobile">
//             <Grid.Column width={1} />
//             <Grid.Column width={6} style={inputLabelStyle}>
//               Name
//             </Grid.Column>
//             <Grid.Column width={8}>
//               <Input placeholder="Enter the Name" fluid onChange={this.handleNameChange} />
//             </Grid.Column>
//             <Grid.Column width={1} />
//           </Grid.Row>
//
//           {/* Mobile Number */}
//           <Grid.Row only="mobile">
//             <Grid.Column width={1} />
//             <Grid.Column width={6} style={inputLabelStyle}>
//               Mobile no.
//             </Grid.Column>
//             <Grid.Column width={8}>
//               <Input
//                 type="number"
//                 label="+42"
//                 placeholder="Mobile number"
//                 fluid
//                 onChange={this.handleMobileNumberChange}
//               />
//             </Grid.Column>
//             <Grid.Column width={1} />
//           </Grid.Row>
//
//           {/* Vendor Location */}
//           <Grid.Row only="mobile">
//             <Grid.Column width={1} />
//             <Grid.Column width={6} style={inputLabelStyle}>
//               Location
//             </Grid.Column>
//             <Grid.Column width={8}>
//               <Input placeholder="Enter the Location" fluid onChange={this.handleLocationChange} />
//             </Grid.Column>
//             <Grid.Column width={1} />
//           </Grid.Row>
//
//           {/* Distance served */}
//           <Grid.Row only="mobile">
//             <Grid.Column width={1} />
//             <Grid.Column width={6} style={inputLabelStyle}>
//               Distance served
//             </Grid.Column>
//             <Grid.Column width={8}>
//               <Form>
//                 <Form.Field>
//                   <Radio
//                     className="radioLabelStyle"
//                     label="within 100km"
//                     name="radioGroup"
//                     value="100km"
//                     checked={this.state.value === '100km'}
//                     onChange={this.handleRadioChange}
//                   />
//                 </Form.Field>
//                 <Form.Field>
//                   <Radio
//                     className="radioLabelStyle"
//                     label="within 200km"
//                     name="radioGroup"
//                     value="200km"
//                     checked={this.state.value === '200km'}
//                     onChange={this.handleRadioChange}
//                   />
//                 </Form.Field>
//               </Form>
//             </Grid.Column>
//             <Grid.Column width={1} />
//           </Grid.Row>
//
//           {/* Label for cost details */}
//           <Grid.Row only="mobile">
//             <Grid.Column width={8}>
//               <Label tag style={labelStyle}>
//                 Cost Details
//               </Label>
//             </Grid.Column>
//             <Grid.Column width={8} />
//           </Grid.Row>
//           {displayCategories}
//
//           {this.state.categoryDisplayCount < 3 ? (
//             <Grid.Row only="mobile">
//               <Grid.Column width={13} />
//               <Grid.Column width={2}>
//                 <FloatingActionButton mini backgroundColor="#cc0000" onClick={this.handleFAB}>
//                   <ContentAdd />
//                 </FloatingActionButton>
//               </Grid.Column>
//               <Grid.Column width={1} />
//             </Grid.Row>
//           ) : null}
//
//           {/* Label for documents */}
//           <Grid.Row only="mobile">
//             <Grid.Column width={8}>
//               <Label tag style={labelStyle}>
//                 Documents
//               </Label>
//             </Grid.Column>
//             <Grid.Column width={8} />
//           </Grid.Row>
//
//           {/* Documents */}
//           {this.state.firstDocumentUploading ? (
//             <Grid.Row only="mobile">
//               <Grid.Column width={1} />
//               <Grid.Column width={6} style={inputLabelStyle}>
//                 License
//               </Grid.Column>
//               <Grid.Column width={8}>
//                 <Button loading label="Uploading" labelPosition="left" icon="upload" fluid />
//               </Grid.Column>
//               <Grid.Column width={1} />
//             </Grid.Row>
//           ) : this.state.firstDocumentUploaded ? (
//             <Grid.Row only="mobile">
//               <Grid.Column width={1} />
//               <Grid.Column width={6} style={inputLabelStyle}>
//                 License
//               </Grid.Column>
//               <Grid.Column width={8}>
//                 <Button
//                   color="green"
//                   label="Uploaded!"
//                   labelPosition="left"
//                   icon="checkmark"
//                   fluid
//                 />
//               </Grid.Column>
//               <Grid.Column width={1} />
//             </Grid.Row>
//           ) : (
//             <Grid.Row only="mobile">
//               <Grid.Column width={1} />
//               <Grid.Column width={6} style={inputLabelStyle}>
//                 License
//               </Grid.Column>
//               <Grid.Column width={8}>
//                 <Button
//                   label="Attach"
//                   labelPosition="left"
//                   icon="upload"
//                   fluid
//                   onClick={this.handleUploadFirstDocument}
//                 />
//               </Grid.Column>
//               <Grid.Column width={1} />
//             </Grid.Row>
//           )}
//
//           {this.state.secondDocumentUploading ? (
//             <Grid.Row only="mobile">
//               <Grid.Column width={1} />
//               <Grid.Column width={6} style={inputLabelStyle}>
//                 Certificate
//               </Grid.Column>
//               <Grid.Column width={8}>
//                 <Button loading label="Uploading" labelPosition="left" icon="upload" fluid />
//               </Grid.Column>
//               <Grid.Column width={1} />
//             </Grid.Row>
//           ) : this.state.secondDocumentUploaded ? (
//             <Grid.Row only="mobile">
//               <Grid.Column width={1} />
//               <Grid.Column width={6} style={inputLabelStyle}>
//                 Certificate
//               </Grid.Column>
//               <Grid.Column width={8}>
//                 <Button
//                   color="green"
//                   label="Uploaded!"
//                   labelPosition="left"
//                   icon="checkmark"
//                   fluid
//                 />
//               </Grid.Column>
//               <Grid.Column width={1} />
//             </Grid.Row>
//           ) : (
//             <Grid.Row only="mobile">
//               <Grid.Column width={1} />
//               <Grid.Column width={6} style={inputLabelStyle}>
//                 Certificate
//               </Grid.Column>
//               <Grid.Column width={8}>
//                 <Button
//                   label="Attach"
//                   labelPosition="left"
//                   icon="upload"
//                   fluid
//                   onClick={this.handleUploadSecondDocument}
//                 />
//               </Grid.Column>
//               <Grid.Column width={1} />
//             </Grid.Row>
//           )}
//
//           {/* Submit */}
//           <Grid.Row only="mobile">
//             <Grid.Column width={1} />
//             <Grid.Column width={14}>
//               <center>
//                 <Button onClick={this.handleSubmit} style={submitButtonStyle}>
//                   SUBMIT
//                 </Button>
//               </center>
//             </Grid.Column>
//             <Grid.Column width={1} />
//           </Grid.Row>
//
//           {/* After submitting the form, this modal will appear */}
//           <Grid.Row only="mobile">
//             <Grid.Column width={1} />
//             <Grid.Column width={14}>
//               <Modal open={this.state.openModal}>
//                 <Header icon="ticket" content="Successfully Registered" />
//                 <Modal.Content>
//                   <h4>
//                     <List>
//                       <List.Item content="Your document verification is pending with us. Once it is done you will get a notification" />
//                     </List>
//                   </h4>
//                   <span style={{ float: 'right' }}>
//                     <Link to="/home/$">
//                       {' '}
//                       <Button
//                         style={{
//                           letterSpacing: '1px',
//                           padding: 12,
//                           marginBottom: 10,
//                         }}
//                         size="tiny"
//                         color="black"
//                         onClick={() => {
//                           this.setState({
//                             openModal: false,
//                           });
//                         }}
//                       >
//                         <Icon name="like outline" /> OK
//                       </Button>
//                     </Link>
//                   </span>
//                 </Modal.Content>
//               </Modal>
//             </Grid.Column>
//             <Grid.Column width={1} />
//           </Grid.Row>
//         </Grid>
//       </div>
//     );
//   }
// }
import React, { Component } from 'react';
import {
  Radio,
  Header,
  Grid,
  Icon,
  List,
  Form,
  Button,
  Input,
  Label,
  Modal,
} from 'semantic-ui-react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import { Link } from 'react-router-dom';
import request from 'superagent';

import AppBar from './AppBarhome.jsx';
import CategoryForVendorRegistration from './CategoryForVendorRegistration.jsx';

// Internal styling
const containerStyle = {
  backgroundImage: "url('./client/Images/bg-red.jpg')",
  backgroundAttachment: 'fixed',
  overflow: 'hidden',
};
const titleStyle = {
  textAlign: 'center',
  fontWeight: '600',
  letterSpacing: '1px',
  fontFamily: 'Open Sans',
  color: 'white',
  fontSize: 20,
  textTransform: 'capitalise',
};
const labelStyle = {
  textTransform: 'capitalise',
  letterSpacing: '2px',
  textAlign: 'center',
  fontWeight: '600',
  fontSize: 14,
  fontFamily: 'Open Sans',
  color: 'white',
  backgroundColor: '#CC0000',
};
const inputLabelStyle = {
  fontFamily: 'Open Sans',
  textTransform: 'capitalise',
  fontSize: 14,
  color: 'white',
  fontWeight: '600',
};
const submitButtonStyle = {
  letterSpacing: '2px',
  boxShadow: '0 6px 10px 0 hsla(0, 0%, 0%, 0.2)',
  fontFamily: 'Open Sans',
  backgroundColor: '#CC0000',
  color: 'white',
  padding: '12px',
};

export default class VendorRegistration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      vendorName: '',
      vendorLocation: '',
      vendorMobileNumber: 0,
      rating: 0,
      mobileInputDisable:false,
      vendorStatus: 'not verified',
      firstDocumentModal: false,
      firstDocumentUploading: false,
      firstDocumentUploaded: false,
      secondDocumentUploading: false,
      secondDocumentUploaded: false,
      openModal: false,
      categoryDisplayCount: 1,
      selectedCategories: [
        {
          category: 'Full Cleaning',
          Bathroom: 0,
          Pantry: 0,
          Room: 0,
          Window: 0,
          AdditionalSqFt: 0,
        },
      ],
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleMobileNumberChange = this.handleMobileNumberChange.bind(this);
    this.handleRadioChange = this.handleRadioChange.bind(this);
    this.handleCategoryChange = this.handleCategoryChange.bind(this);
    this.handleUploadFirstDocument = this.handleUploadFirstDocument.bind(this);
    this.handleUploadSecondDocument = this.handleUploadSecondDocument.bind(this);
    this.handleFAB = this.handleFAB.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  // set the vendor name in state
  handleNameChange = (e) => {
    this.setState({ vendorName: e.target.value });
  };
  // set the vendor location in state
  handleLocationChange = (e) => {
    this.setState({ vendorLocation: e.target.value });
  };
  // set the vendor location in state
  handleMobileNumberChange = (e) => {
    this.setState({ vendorMobileNumber: e.target.value });
    if(e.target.value.length == 9){
      this.setState({mobileInputDisable:true})
    }
  };

  handleCategoryChange(value, index) {
    const { selectedCategories } = this.state;
    if (!selectedCategories.find(cat => value == cat.category)) {
      if (value === 'Full Cleaning') {
        selectedCategories.splice(index, 1, {
          category: value,
          Bathroom: 0,
          Pantry: 0,
          Room: 0,
          Window: 0,
          AdditionalSqFt: 0,
        });
      } else if (value === 'Deep Cleaning') {
        selectedCategories.splice(index, 1, {
          category: value,
          Floor: 0,
          Window: 0,
          Door: 0,
          Bathroom: 0,
          Pantry: 0,
          Room: 0,
          AdditionalSqFt: 0,
        });
      } else if (value === 'Duct Cleaning') {
        selectedCategories.splice(index, 1, {
          category: value,
          AreaInSqFt: 0,
        });
      }
    } else {
      alert('Category already exists. Select a different category');
    }

    this.setState(
      {
        selectedCategories,
      },
      () => {
        console.log(selectedCategories);
      },
    );
  }

  handleValueChange(value, index, fieldName) {
    const { selectedCategories } = this.state;
    const category = selectedCategories[index];
    category[fieldName] = value;
    selectedCategories.splice(index, 1, category);
    this.setState({ selectedCategories }, () => {
      console.log('handleValueChange:', selectedCategories);
    });
  }

  // For handling the radio button in the form
  handleRadioChange = (e, { value }) => this.setState({ value });

  // Below function passes the form data from client to server side
  handleSubmit = () => {
    // Generate the random values between 3.5 and 5.0
    const context = this;
    this.setState({
      openModal: true,
      vendorStatus: 'not verified',
    });

    request
      .post('/vendorRequest')
      .query({
        vendor_name: this.state.vendorName,
        vendor_location: this.state.vendorLocation,
        vendor_mobilenumber: this.state.vendorMobileNumber,
        vendor_category: this.state.selectedCategories,
        vendor_distance: this.state.value,
        vendor_status: this.state.vendorStatus,
      })
      .end((err, res) => {
        console.log(res, 'response from db');
      });
    // console.log(this.state.selectedCategories,"In client side");
  };

  // Uploading the first document in the form
  handleUploadFirstDocument = () => {
    this.setState({ firstDocumentUploading: true }, () => {
      const timerId = setInterval(() => {
        this.setState(
          {
            firstDocumentUploading: false,
            firstDocumentUploaded: true,
          },
          () => {
            clearInterval(timerId);
          },
        );
      }, 2000);
    });
  };

  // Uploading the second document in the form
  handleUploadSecondDocument = () => {
    this.setState({ secondDocumentUploading: true }, () => {
      const timerId = setInterval(() => {
        this.setState(
          {
            secondDocumentUploading: false,
            secondDocumentUploaded: true,
          },
          () => {
            clearInterval(timerId);
          },
        );
      }, 2000);
    });
  };

  // Floating Action Button -> used to add more category
  handleFAB = () => {
    let { categoryDisplayCount, selectedCategories } = this.state;
    if (categoryDisplayCount < 3) {
      categoryDisplayCount++;
    }
    selectedCategories.push({
      category: 'Full Cleaning',
      Bathroom: '',
      Pantry: '',
      Room: '',
      Window: '',
      AdditionalSqFt: '',
    });
    this.setState(
      {
        categoryDisplayCount,
        selectedCategories,
      },
      () => {
        console.log(selectedCategories);
      },
    );
  };

  render() {
    const displayCategories = [];
    for (let i = 0; i < this.state.categoryDisplayCount; i++) {
      displayCategories.push(<CategoryForVendorRegistration
        index={i}
        handleCategoryChange={value => this.handleCategoryChange(value, i)}
        handleValueChange={(value, index, fieldName) =>
            this.handleValueChange(value, index, fieldName)
          }
        selectedCategories={this.state.selectedCategories}
      />);
    }
    // console.log(this.state.vendorName, 'vendor name');
    // console.log(this.state.vendorLocation, 'vendor location');
    // console.log(this.state.rating, 'rating');
    // console.log(this.state.selectedCategories, 'category in frontend');
    return (
      <div style={containerStyle}>
        <AppBar />
        <Grid>
          <Grid.Row only="mobile">
            <Grid.Column width={2} />
            <Grid.Column width={12}>
              <p style={titleStyle}>Vendor Registration Form</p>
            </Grid.Column>
            <Grid.Column width={2} />
          </Grid.Row>

          {/* Label for details */}
          <Grid.Row only="mobile">
            <Grid.Column width={8}>
              <Label tag style={labelStyle}>
                Details
              </Label>
            </Grid.Column>
            <Grid.Column width={8} />
          </Grid.Row>

          {/* Vendor Name */}
          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6} style={inputLabelStyle}>
              Name
            </Grid.Column>
            <Grid.Column width={8}>
              <Input placeholder="Enter the Name" fluid onChange={this.handleNameChange} />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          {/* Mobile Number */}
          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6} style={inputLabelStyle}>
              Mobile no.
            </Grid.Column>
            <Grid.Column width={8}>
              <Input
                type="number"
                disabled={this.state.mobileInputDisable}
                label="+42"
                placeholder="Mobile number"
                fluid
                onChange={this.handleMobileNumberChange}
              />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          {/* Vendor Location */}
          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6} style={inputLabelStyle}>
              Location
            </Grid.Column>
            <Grid.Column width={8}>
              <Input placeholder="Enter the Location" fluid onChange={this.handleLocationChange} />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          {/* Distance served */}
          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6} style={inputLabelStyle}>
              Distance served
            </Grid.Column>
            <Grid.Column width={8}>
              <Form>
                <Form.Field>
                  <Radio
                    className="radioLabelStyle"
                    label="within 100km"
                    name="radioGroup"
                    value="100km"
                    checked={this.state.value === '100km'}
                    onChange={this.handleRadioChange}
                  />
                </Form.Field>
                <Form.Field>
                  <Radio
                    className="radioLabelStyle"
                    label="within 200km"
                    name="radioGroup"
                    value="200km"
                    checked={this.state.value === '200km'}
                    onChange={this.handleRadioChange}
                  />
                </Form.Field>
              </Form>
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          {/* Label for cost details */}
          <Grid.Row only="mobile">
            <Grid.Column width={8}>
              <Label tag style={labelStyle}>
                Cost Details
              </Label>
            </Grid.Column>
            <Grid.Column width={8} />
          </Grid.Row>
          {displayCategories}

          {this.state.categoryDisplayCount < 3 ? (
            <Grid.Row only="mobile">
              <Grid.Column width={13} />
              <Grid.Column width={2}>
                <FloatingActionButton mini backgroundColor="#cc0000" onClick={this.handleFAB}>
                  <ContentAdd />
                </FloatingActionButton>
              </Grid.Column>
              <Grid.Column width={1} />
            </Grid.Row>
          ) : null}

          {/* Label for documents */}
          <Grid.Row only="mobile">
            <Grid.Column width={8}>
              <Label tag style={labelStyle}>
                Documents
              </Label>
            </Grid.Column>
            <Grid.Column width={8} />
          </Grid.Row>

          {/* Documents */}
          {this.state.firstDocumentUploading ? (
            <Grid.Row only="mobile">
              <Grid.Column width={1} />
              <Grid.Column width={6} style={inputLabelStyle}>
                License
              </Grid.Column>
              <Grid.Column width={8}>
                <Button loading label="Uploading" labelPosition="left" icon="upload" fluid />
              </Grid.Column>
              <Grid.Column width={1} />
            </Grid.Row>
          ) : this.state.firstDocumentUploaded ? (
            <Grid.Row only="mobile">
              <Grid.Column width={1} />
              <Grid.Column width={6} style={inputLabelStyle}>
                License
              </Grid.Column>
              <Grid.Column width={8}>
                <Button
                  color="green"
                  label="Uploaded!"
                  labelPosition="left"
                  icon="checkmark"
                  fluid
                />
              </Grid.Column>
              <Grid.Column width={1} />
            </Grid.Row>
          ) : (
            <Grid.Row only="mobile">
              <Grid.Column width={1} />
              <Grid.Column width={6} style={inputLabelStyle}>
                License
              </Grid.Column>
              <Grid.Column width={8}>
                <Button
                  label="Attach"
                  labelPosition="left"
                  icon="upload"
                  fluid
                  onClick={this.handleUploadFirstDocument}
                />
              </Grid.Column>
              <Grid.Column width={1} />
            </Grid.Row>
          )}

          {this.state.secondDocumentUploading ? (
            <Grid.Row only="mobile">
              <Grid.Column width={1} />
              <Grid.Column width={6} style={inputLabelStyle}>
                Certificate
              </Grid.Column>
              <Grid.Column width={8}>
                <Button loading label="Uploading" labelPosition="left" icon="upload" fluid />
              </Grid.Column>
              <Grid.Column width={1} />
            </Grid.Row>
          ) : this.state.secondDocumentUploaded ? (
            <Grid.Row only="mobile">
              <Grid.Column width={1} />
              <Grid.Column width={6} style={inputLabelStyle}>
                Certificate
              </Grid.Column>
              <Grid.Column width={8}>
                <Button
                  color="green"
                  label="Uploaded!"
                  labelPosition="left"
                  icon="checkmark"
                  fluid
                />
              </Grid.Column>
              <Grid.Column width={1} />
            </Grid.Row>
          ) : (
            <Grid.Row only="mobile">
              <Grid.Column width={1} />
              <Grid.Column width={6} style={inputLabelStyle}>
                Certificate
              </Grid.Column>
              <Grid.Column width={8}>
                <Button
                  label="Attach"
                  labelPosition="left"
                  icon="upload"
                  fluid
                  onClick={this.handleUploadSecondDocument}
                />
              </Grid.Column>
              <Grid.Column width={1} />
            </Grid.Row>
          )}

          {/* Submit */}
          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={14}>
              <center>
                <Button onClick={this.handleSubmit} style={submitButtonStyle}>
                  SUBMIT
                </Button>
              </center>
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          {/* After submitting the form, this modal will appear */}
          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={14}>
              <Modal open={this.state.openModal}>
                <Header icon="ticket" content="Successfully Registered" />
                <Modal.Content>
                  <h4>
                    <List>
                      <List.Item content="Your document verification is pending with us. Once it is done you will get a notification" />
                    </List>
                  </h4>
                  <span style={{ float: 'right' }}>
                    <Link to="/home/$">
                      {' '}
                      <Button
                        style={{
                          letterSpacing: '1px',
                          padding: 12,
                          marginBottom: 10,
                        }}
                        size="tiny"
                        color="black"
                        onClick={() => {
                          this.setState({
                            openModal: false,
                          });
                        }}
                      >
                        <Icon name="like outline" /> OK
                      </Button>
                    </Link>
                  </span>
                </Modal.Content>
              </Modal>
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}
