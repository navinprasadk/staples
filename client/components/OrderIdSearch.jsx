import React, { Component } from 'react';
import { Grid, Input, Button, Icon, Menu, Form, Dropdown,List } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import SwipeableViews from 'react-swipeable-views';
import AppBar from './AppBarhome.jsx';
import Request from 'superagent';

export default class OrderidSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index:'',
      activeItem:'Submitted',
      allorder:[],
      searchbyOrderId:[],
      dateValueTransit:''
    };
  }
  componentWillMount(){
    var context = this;
    Request.get('/searchbyOrderId')
            .end((err, res)=>{
              if(err){
                console.log('Error--------->',err);
              }
              else{
                console.log('to check for data',res.body);
                this.setState({searchbyOrderId:res.body})
              }
            })
            var today = new Date();
    today.setDate(today.getDate());
    var a = today.toString().slice(4, 16);
    var date = new Date(a).toISOString().substring(0,10);
    this.setState({
      dateValueTransit: date
    });
        }

  render() {
    console.log('awsedrcguh',this.state.searchbyOrderId);
    const { activeItem } = this.state;
    return (
      <div>
        <AppBar/>
        <Grid>
          <Grid.Row>
            <Grid.Column width={16}>
              <center>
                <h3>Order Details</h3>
              </center>
            </Grid.Column>
            </Grid.Row>
          </Grid>
            {this.state.searchbyOrderId.map((item,key) => {
              return(
                <Grid>
                <Grid.Row key={key}>
                  <Grid.Column width={2}/>
                  <Grid.Column width={6}>
                      <h3>Order ID</h3>
                  </Grid.Column>
                  <Grid.Column width={1}>
                    <h3>:</h3>
                  </Grid.Column>
                  <Grid.Column width={5}>
                      <h3>{item.so_id}</h3>
                  </Grid.Column>
                  <Grid.Column width={2}/>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={2}/>
                  <Grid.Column width={6}>
                      <h3>Company ID</h3>
                  </Grid.Column>
                  <Grid.Column width={1}>
                    <h3>:</h3>
                  </Grid.Column>
                  <Grid.Column width={5}>
                      <h3>{item.customer_id}</h3>
                  </Grid.Column>
                  <Grid.Column width={2}/>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={2}/>
                  <Grid.Column width={6}>
                      <h3>Company Address</h3>
                  </Grid.Column>
                  <Grid.Column width={1}>
                    <h3>:</h3>
                  </Grid.Column>
                  <Grid.Column width={5}>
                      <h3>{item.customer_location}</h3>
                  </Grid.Column>
                  <Grid.Column width={2}/>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={2}/>
                  <Grid.Column width={6}>
                      <h3>Order Submitted</h3>
                  </Grid.Column>
                  <Grid.Column width={1}>
                    <h3>:</h3>
                  </Grid.Column>
                  <Grid.Column width={5}>
                      <h3>{this.state.dateValueTransit}</h3>
                  </Grid.Column>
                  <Grid.Column width={2}/>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={2}/>
                  <Grid.Column width={6}>
                      <h3>Service Requested</h3>
                  </Grid.Column>
                  <Grid.Column width={1}>
                    <h3>:</h3>
                  </Grid.Column>
                  <Grid.Column width={5}>
                      <h3>{new Date(item.request_date).toISOString().substring(0,10)}</h3>
                  </Grid.Column>
                  <Grid.Column width={2}/>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={2}/>
                  <Grid.Column width={6}>
                      <h3>Service Type</h3>
                  </Grid.Column>
                  <Grid.Column width={1}>
                    <h3>:</h3>
                  </Grid.Column>
                  <Grid.Column width={5}>
                      <h3>{item.service_category_name}</h3>
                  </Grid.Column>
                  <Grid.Column width={2}/>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={2}/>
                  <Grid.Column width={6}>
                      <h3>Total Charges </h3>
                  </Grid.Column>
                  <Grid.Column width={1}>
                    <h3>:</h3>
                  </Grid.Column>
                  <Grid.Column width={5}>
                      <h3>{item.staples_quote}</h3>
                  </Grid.Column>
                  <Grid.Column width={2}/>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={2}/>
                  <Grid.Column width={6}>
                      <h3>Vendor Name</h3>
                  </Grid.Column>
                  <Grid.Column width={1}>
                    <h3>:</h3>
                  </Grid.Column>
                  <Grid.Column width={5}>
                      <h3>{item.vendor_name}</h3>
                  </Grid.Column>
                  <Grid.Column width={2}/>
                </Grid.Row>
              </Grid>
              )
            })}
            <Grid>
              <Grid.Row>
                <Grid.Column>
                  <center>
                    <Button as={Link} to='/ordersearch'>Back</Button>
                  </center>
                </Grid.Column>
              </Grid.Row>
            </Grid>
      </div>
    );
  }
}
