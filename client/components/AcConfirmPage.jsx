import React, { Component } from 'react';
import {
  Grid,
  Button,
  Segment,
  Dimmer,
  Modal,
  Header,
  Loader,
  Icon,
  List,
} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import AppBarhome from './AppBarhome.jsx';
import Request from 'superagent';


export default class Confirm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cleaningType: '',
      totalValue:0,
      // numberOfFloors: 0,
      numberOfWindows: ' ',
      numberOfPantry: 0,
      numberOfBathroom: 0,
      area: '',
      displayDate: '',
      displayTime: '',
      address: '',
      openModal: false,
      loaderStatus: true,
      noofArea:0
      // date: ''
    };
  }
  componentDidMount() {
    if(this.state.cleaningType == 'AC Maintenance'){
      var area;
    var noofArea;
    if(this.state.area == 'less than 500' ){
      console.log(',,,,,,,,,,,,,,,,,,');
      noofArea = 1 ;
       area = 1000 * 1;
    }
    else if(this.state.area == '500 to 1000'){
      noofArea = 2;
       area = 1000 * 2;
    }
    else if(this.state.area == '1000 to 1500'){
      noofArea = 3;
       area = 1000 * 3;
    }
    else if(this.state.area == '1500 to 2000'){
      noofArea = 4;
      area = 1000 * 4;
    }
    else{
      noofArea = 5
       area = 1000 * 5;
    }

    var totalValue = area;
    this.setState({totalValue:totalValue,noofArea:noofArea})
}
    else{
      var noofArea;
      var area1;
    if(this.state.area == 'less than 500' ){
      noofArea = 1 ;
       area1 = 1200 * 1;
    }
    else if(this.state.area == '500 to 1000'){
      noofArea = 2;
       area1 = 1200 * 2;
    }
    else if(this.state.area == '1000 to 1500'){
      noofArea = 3;
       area1 = 1200 * 3;
    }
    else if(this.state.area == '1500 to 2000'){
      noofArea = 4;
      area1 = 1200 * 4;
    }
    else{
      noofArea = 5;
       area1 = 1200 * 5;
    }

    var totalValue = area1;
    this.setState({totalValue:totalValue,noofArea:noofArea})
  }
    setTimeout(() => {
      this.setState({
        loaderStatus: false,
      });
    }, 2000);
  }
  componentWillMount() {
    const {
      cleaningType,
      // numberOfFloors,
      // numberOfWindows,
      // numberOfPantry,
      // numberOfBathroom,
      area,
      displayDate,
      displayTime,
      address,
      // date
    } =
      this.props.location.state || this.state;
    this.setState({
      cleaningType,
      // numberOfFloors,
      // numberOfWindows,
      // numberOfPantry,
      // numberOfBathroom,
      area,
      displayDate,
      displayTime,
      address,
      // date
    });

  }

serviceSubmiteDetails(e){
  console.log('srexdtfgy inside --------->',e);
  Request.post('/serviceRequest')
  .query({cleaningType:this.state.cleaningType,noofArea:this.state.noofArea,displayDate:this.state.displayDate,
    address:this.state.address,quote:e})
        .end((err, res) => {
          if (err) {
            console.log('Error from fetching bookingOrder data - > ',err);
          } else {
            // console.log('response from bookingOrder data - > ',JSON.parse(res.text));
            // this.setState({boDetails:JSON.parse(res.text)});
            // console.log('state - > ',this.state.boDetails);
          }
        });
  this.setState({openModal: true});
}
  render() {

    return (
      <div
        className="container"
        style={{
          backgroundImage: "url('./client/Images/bg-red.jpg')",
          overflow: 'hidden',
          height: '100vh',
        }}
        // style={{ overflow: 'hidden' }}
      >
        {this.state.loaderStatus ? (
          <Grid>
            <Dimmer active>
              <Loader>Loading...</Loader>
            </Dimmer>
          </Grid>
        ) : (
          <div>
            <AppBarhome />
            {/* <Segment raised style={{ marginTop: '-5%' }}> */}
            <Grid style={{ marginTop: '5%', marginLeft: '5%' }} columns={2}>
              <Grid.Row style={{ marginBottom: '5%' }}>
                <Grid.Column>
                  <h4
                    style={{
                      opacity: 0.85,
                      fontFamily: 'Open Sans',
                      color: '#fff',
                      letterSpacing: '1px',
                    }}
                  >
                    DATE AND TIME
                  </h4>
                  <h4 style={{ fontFamily: 'Open Sans', color: '#fff', marginTop: '-2%' }}>
                    <div>{this.state.displayDate},</div>
                    <div> {this.state.displayTime}</div>
                  </h4>
                </Grid.Column>
                <Grid.Column>
                  <h4
                    style={{
                      opacity: 0.85,
                      fontFamily: 'Open Sans',
                      color: '#fff',
                      letterSpacing: '1px',
                    }}
                  >
                    SERVICE TYPE
                  </h4>
                  <h4 style={{ fontFamily: 'Open Sans', color: '#fff', marginTop: '-2%' }}>
                    {this.state.cleaningType}{' '}
                  </h4>
                </Grid.Column>
              </Grid.Row>

              <Grid.Row style={{ marginBottom: '5%' }}>

                <Grid.Column>
                  <h4
                    style={{
                      opacity: 0.85,
                      fontFamily: 'Open Sans',
                      color: '#fff',
                      letterSpacing: '1px',
                    }}
                  >
                    AREA IN SQ.FT
                  </h4>
                  <h4 style={{ fontFamily: 'Open Sans', color: '#fff', marginTop: '-2%' }}>
                    {this.state.area}{' '}
                  </h4>
                </Grid.Column>
              </Grid.Row>
              {/* </Grid> */}

              {/* <Grid> */}
              <Grid.Row style={{ marginBottom: '5%' }}>
                <Grid.Column>
                  <h4
                    style={{
                      opacity: 0.85,
                      fontFamily: 'Open Sans',
                      color: '#fff',
                      letterSpacing: '1px',
                    }}
                  >
                    ADDRESS
                  </h4>
                  <h4 style={{ fontFamily: 'Open Sans', color: '#fff', marginTop: '-2%' }}>
                    {this.state.address}
                  </h4>
                </Grid.Column>
                <Grid.Column>
                  <h4
                    style={{
                      opacity: 0.85,
                      fontFamily: 'Open Sans',
                      color: '#fff',
                      letterSpacing: '1px',
                    }}
                  >
                    ESTIMATE
                  </h4>
                  <h4 style={{ fontFamily: 'Open Sans', color: '#fff', marginTop: '-2%' }}>
                      <span>
                        <Icon name="euro" />{this.state.totalValue}
                      </span>
                  </h4>
                </Grid.Column>
              </Grid.Row>
            </Grid>
            {/* </Segment> */}

            <Grid>
              <Grid.Row only="mobile" style={{ marginBottom: '10%' }}>
                <Grid.Column width={2} />
                <Grid.Column style={{ marginTop: '5%' }} width={6}>
                  <center>
                    {/* <Link to="/OrderMessage"> */}
                    <Link
                      to={{
                        pathname: '/acCleaning',
                        state: {
                          cleaningType: this.state.cleaningType,
                          // numberOfFloors: this.state.numberOfFloors,
                          // numberOfWindows: this.state.numberOfWindows,
                          // numberOfPantry: this.state.numberOfPantry,
                          // numberOfBathroom: this.state.numberOfBathroom,
                          area: this.state.area,
                          displayDate: this.state.displayDate,
                          displayTime: this.state.displayTime,
                          address: this.state.address,
                          // date: this.state.date
                        },
                      }}
                    >
                      <Button
                        inverted
                        className="box-shadow-silver"
                        style={{
                          //   backgroundColor: 'silver',
                          color: '#fff',
                          letterSpacing: '2px',
                          fontFamily: 'Open Sans',
                        }}
                      >
                        EDIT
                      </Button>
                    </Link>
                    {/* </Link> */}
                  </center>
                </Grid.Column>
                <Grid.Column style={{ marginTop: '5%' }} width={6}>
                  <center>
                    {/* <Link to="/OrderMessage"> */}
                    <Button
                      className="box-shadow-silver"
                      style={{
                        backgroundColor: '#fff',
                        color: '#000',
                        letterSpacing: '2px',
                        fontFamily: 'Open Sans',
                      }}
                      onClick={this.serviceSubmiteDetails.bind(this,this.state.totalValue)}
                    >
                      SUBMIT
                    </Button>
                    {/* </Link> */}
                  </center>
                </Grid.Column>
                <Grid.Column width={2} />
              </Grid.Row>
            </Grid>
            <Modal open={this.state.openModal}>
              <Header icon="erase" content="Service Confirmed" />
              <Modal.Content>
                <h4>
                  {/* Your service request has been placed successfully and will be processed within two
              hours */}
                  <List>
                    <List.Item
                      icon="checkmark"
                      content="Your service request has been placed successfully"
                    />
                    <List.Item
                      icon="checkmark"
                      content="Vendor will be assigned within two hours and you will be notified"
                    />
                  </List>
                </h4>
              </Modal.Content>
              <Modal.Actions>
                <Link to={`/home/$`}>
                  {' '}
                  <Button
                    size="tiny"
                    color="green"
                    onClick={() => {
                      this.setState({
                        openModal: false,
                      });
                    }}
                  >
                    <Icon name="like outline" /> OK
                  </Button>
                </Link>
              </Modal.Actions>
            </Modal>
          </div>
        )}
      </div>
    );
  }
}
Confirm.propTypes = {
  cleaningType: PropTypes.object,
  // numberOfFloors: PropTypes.object,
  // numberOfWindows: PropTypes.object,
  // numberOfPantry: PropTypes.object,
  // numberOfBathroom: PropTypes.object,
  area: PropTypes.object,
  displayDate: PropTypes.object,
  address: PropTypes.object,
};
