import React, { Component } from 'react';
import { Dropdown, Grid, TextArea, Form, Button, List } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import DatePicker from 'material-ui/DatePicker';

import AppBarhome from './AppBarhome.jsx';

export default class CleaningDetails extends Component {
  constructor(props) {
    super(props);
    const minDate = new Date();
    const maxDate = new Date();
    minDate.setFullYear(minDate.getFullYear());
    minDate.setHours(0, 0, 0, 0);
    maxDate.setFullYear(maxDate.getFullYear() + 1);
    maxDate.setHours(0, 0, 0, 0);

    this.state = {
      cleaningType: '',
      // numberOfFloors: null,
      numberOfWindows: null,
      numberOfPantry: null,
      numberOfBathroom: null,
      area: null,
      address: null,
      minDate,
      maxDate,
      date: null,
      autoOk: false,
      disableYearSelection: false,
      displayDate: null,
      displayTime: null,
      numberOfWindows: null,
    };
  }

  componentWillMount() {
    const {
      cleaningType,
      // numberOfFloors,
      numberOfWindows,
      numberOfPantry,
      numberOfBathroom,
      area,
      displayDate,
      displayTime,
      address,
      date,
    } =
      this.props.location.state || this.state;
    this.setState({
      cleaningType,
      // numberOfFloors,
      numberOfWindows,
      numberOfPantry,
      numberOfBathroom,
      area,
      displayDate,
      displayTime,
      address,
      date,
    });
  }

  handleChangeMinDate = (event, date) => {
    this.setState({
      minDate: date,
    });
  };
  handleDateChange = (event, date) => {
    console.log('///////////',typeof date);
    const displayDate = new Date(new Date(date).setDate(date.getDate()+1)).toISOString().substring(0,10);
    // const displayDate = `${date.toString().substring(11, 15)}-${date
    //   .toISOString()
    //   .substring(4, 7)}-${date.toString().substring(8, 11)}`;

    this.setState({
      date,
      displayDate,
    });

    console.log(displayDate);
  };
  handleChangeMaxDate = (event, date) => {
    this.setState({
      maxDate: date,
    });
  };
  render() {
    console.log('to check guest value',this.props.Guest);
    const cleaningTypeOptions = [
      {
        text: 'Full Cleaning',
        key: 'Full Cleaning',
        value: 'Full Cleaning',
      },
      {
        text: 'Deep Cleaning',
        key: 'Deep Cleaning',
        value: 'Deep Cleaning',
      },
      // {
      //   text: 'Complete Cleaning',
      //   key: 'Complete Cleaning',
      //   value: 'Complete Cleaning',
      // },
    ];
    const numberOptions = [
      {
        text: '1',
        value: 1,
      },
      {
        text: '2',
        value: 2,
      },
      {
        text: '3',
        value: 3,
      },
      {
        text: '4',
        value: 4,
      },
      {
        text: '5',
        value: 5,
      },
      {
        text: '6',
        value: 6,
      },
    ];
    const windowOptions = [
      {
        text: 'less than 10',
        value: 'less than 10',
      },
      {
        text: '10 to 20',
        value: '10 to 20',
      },
      {
        text: '20 to 30 ',
        value: '20 to 30',
      },
      {
        text: 'more than 30',
        value: 'more than 30',
      },
    ];
    const areaOptions = [
      {
        text: 'less than 1000',
        value: 'less than 1000',
      },
      {
        text: '1000 to 2000',
        value: '1000 to 2000',
      },
      {
        text: '2000 to 3000 ',
        value: '2000 to 3000',
      },
      {
        text: '3000 to 4000',
        value: '3000 to 4000',
      },
      {
        text: 'more than 5000 ',
        value: 'more than 5000',
      },
    ];
    const dateOptions = [
      {
        text: '00:00 to 05:00 Hrs',
        value: '00:00 to 05:00 Hrs',
      },
      {
        text: '03:00 to 08:00 Hrs',
        value: '03:00 to 08:00 Hrs',
      },
      {
        text: '08:00 to 13:00 Hrs',
        value: '08:00 to 13:00 Hrs',
      },
      {
        text: '12:00 to 17:00 Hrs',
        value: '12:00 to 17:00 Hrs',
      },
      {
        text: '17:00 to 22:00 Hrs',
        value: '17:00 to 22:00 Hrs',
      },
      {
        text: '19:00 to 24:00 Hrs',
        value: '19:00 to 24:00 Hrs',
      },
    ];
    return (
      <div
        style={{
          backgroundImage: "url('./client/Images/bg-red.jpg')",
          height: '140vh',
          overflow: 'hidden',
        }}
        className="bg-color-request-details container"
      >
        <AppBarhome />
        <Grid>
          <Grid.Row style={{ marginTop: '5%' }} only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6}>
              <h4 style={{ color: '#fff', fontFamily: 'Open Sans' }}>Cleaning Type</h4>
            </Grid.Column>
            <Grid.Column width={8}>
              <Dropdown
                placeholder="Select"
                selection
                fluid
                options={cleaningTypeOptions}
                value={this.state.cleaningType}
                onChange={(e, selected) => {
                  this.setState({ cleaningType: selected.value });
                }}
              />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          {this.state.cleaningType !== '' ? (
            <Grid.Row style={{ marginTop: '-4%', marginLeft: '2%' }} only="mobile">
              <Grid.Column width={1} />
              {/* <Grid.Column width={6} /> */}
              <Grid.Column width={14} style={{ color: 'white' }}>
                {this.state.cleaningType === 'Deep Cleaning' ? (
                  <List>
                    <List.Item
                      style={{ fontFamily: 'Open Sans' }}
                      icon="checkmark"
                      content="Corner to Corner Wet Wiping "
                    />
                    <List.Item
                      style={{ fontFamily: 'Open Sans' }}
                      icon="checkmark"
                      content="Wet vacuuming post shampoo using biodegradable solution"
                    />
                    <List.Item
                      style={{ fontFamily: 'Open Sans' }}
                      icon="checkmark"
                      content="Windows cleaning with environment safe Chemicals"
                    />
                    <List.Item
                      style={{ fontFamily: 'Open Sans' }}
                      icon="checkmark"
                      content="Sparkling clean Pantry"
                    />
                    <List.Item
                      style={{ fontFamily: 'Open Sans' }}
                      icon="checkmark"
                      content="Complete bathroom cleaning with safe cleaning materials"
                    />
                  </List>
                ) : (
                  <List>
                    <List.Item
                      style={{ fontFamily: 'Open Sans' }}
                      icon="checkmark"
                      content="Corner to Corner Dust removal"
                    />
                    <List.Item
                      style={{ fontFamily: 'Open Sans' }}
                      icon="checkmark"
                      content="Dry carpet vacuuming to remove dust"
                    />
                    <List.Item
                      style={{ fontFamily: 'Open Sans' }}
                      icon="checkmark"
                      content="Windows dirt removal"
                    />
                    <List.Item
                      style={{ fontFamily: 'Open Sans' }}
                      icon="checkmark"
                      content="Pantry cleaning"
                    />
                    <List.Item
                      style={{ fontFamily: 'Open Sans' }}
                      icon="checkmark"
                      content="Bathroom cleaning with safe cleaning materials"
                    />
                  </List>
                )}
              </Grid.Column>
              <Grid.Column width={1} />
            </Grid.Row>
          ) : null}

          {/* <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6} style={{ fontFamily: 'Open Sans' }}>
              <h4 style={{ color: '#fff', fontFamily: 'Open Sans' }}>Floors</h4>
            </Grid.Column>
            <Grid.Column width={8}>
              <Dropdown
                placeholder="Number"
                selection
                fluid
                options={numberOptions}
                value={this.state.numberOfFloors}
                onChange={(e, selected) => {
                  this.setState({ numberOfFloors: selected.value });
                }}
              />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row> */}

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6} style={{ color: '#fff', fontFamily: 'Open Sans' }}>
              <h4 style={{ fontFamily: 'Open Sans' }}>Carpet Area</h4>
            </Grid.Column>
            <Grid.Column width={8}>
              <Dropdown
                placeholder="In Square Feet"
                selection
                fluid
                options={areaOptions}
                value={this.state.area}
                onChange={(e, selected) => {
                  this.setState({ area: selected.value });
                }}
              />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6} style={{ fontFamily: 'Open Sans' }}>
              <h4 style={{ color: '#fff', fontFamily: 'Open Sans' }}> Windows</h4>
            </Grid.Column>
            <Grid.Column width={8}>
              <Dropdown
                placeholder="Select"
                selection
                fluid
                options={windowOptions}
                value={this.state.numberOfWindows}
                onChange={(e, selected) => {
                  this.setState({ numberOfWindows: selected.value });
                }}
              />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6} style={{ color: '#fff', fontFamily: 'Open Sans' }}>
              <h4 style={{ fontFamily: 'Open Sans' }}> Pantry</h4>
            </Grid.Column>
            <Grid.Column width={8}>
              <Dropdown
                placeholder="Number"
                selection
                fluid
                options={numberOptions}
                value={this.state.numberOfPantry}
                onChange={(e, selected) => {
                  this.setState({ numberOfPantry: selected.value });
                }}
              />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6} style={{ color: '#fff', fontFamily: 'Open Sans' }}>
              <h4 style={{ fontFamily: 'Open Sans' }}>Bathrooms</h4>
            </Grid.Column>
            <Grid.Column width={8}>
              <Dropdown
                placeholder="Number"
                selection
                fluid
                options={numberOptions}
                value={this.state.numberOfBathroom}
                onChange={(e, selected) => {
                  this.setState({ numberOfBathroom: selected.value });
                }}
              />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column
              width={6}
              style={{ color: '#fff', fontFamily: 'Open Sans', marginTop: '10%' }}
            >
              <h4 style={{ fontFamily: 'Open Sans' }}> Date</h4>
            </Grid.Column>
            <Grid.Column width={8}>
              <DatePicker
                floatingLabelText="Select"
                className="Date1"
                autoOk={this.state.autoOk}
                minDate={this.state.minDate}
                maxDate={this.state.maxDate}
                disableYearSelection={this.state.disableYearSelection}
                value={this.state.date}
                onChange={this.handleDateChange}
              />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6} style={{ color: '#fff', fontFamily: 'Open Sans' }}>
              <h4 style={{ fontFamily: 'Open Sans' }}> Time Slot</h4>
            </Grid.Column>
            <Grid.Column width={8}>
              <Dropdown
                placeholder="Select"
                selection
                fluid
                options={dateOptions}
                value={this.state.displayTime}
                onChange={(e, selected) => {
                  this.setState({ displayTime: selected.value });
                }}
              />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6} style={{ fontFamily: 'Open Sans' }}>
              <h4 style={{ color: '#fff', fontFamily: 'Open Sans' }}> Address</h4>
            </Grid.Column>
            <Grid.Column width={8}>
              <Form>
                {' '}
                <TextArea
                  placeholder="Select"
                  onChange={(e) => {
                    this.setState({ address: e.target.value });
                  }}
                />
              </Form>
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={2} />

            <Grid.Column width={12}>
              <center>
                {this.props.match.params.value == 'false' ? <Link
                  to={{
                    pathname: `/ConfirmationGuest/${this.props.match.params.value}`,
                    state: {
                      cleaningType: this.state.cleaningType,
                      // numberOfFloors: this.state.numberOfFloors,
                      numberOfWindows: this.state.numberOfWindows,
                      numberOfPantry: this.state.numberOfPantry,
                      numberOfBathroom: this.state.numberOfBathroom,
                      area: this.state.area,
                      displayDate: this.state.displayDate,
                      displayTime: this.state.displayTime,
                      address: this.state.address,
                      date: this.state.date,
                    },
                  }}
                >
                  <Button
                    className="box-shadow-silver"
                    // className="box-shadow"
                    style={{
                      backgroundColor: '#fff',
                      color: '#000',
                      letterSpacing: '3px',
                      fontFamily: 'Open Sans',
                    }}
                  >
                    NEXT
                  </Button>
                </Link>:<Link
                  to={{
                    pathname: `/ConfirmRegistered/${this.props.match.params.value}`,
                    state: {
                      cleaningType: this.state.cleaningType,
                      // numberOfFloors: this.state.numberOfFloors,
                      numberOfWindows: this.state.numberOfWindows,
                      numberOfPantry: this.state.numberOfPantry,
                      numberOfBathroom: this.state.numberOfBathroom,
                      area: this.state.area,
                      displayDate: this.state.displayDate,
                      displayTime: this.state.displayTime,
                      address: this.state.address,
                      date: this.state.date,
                    },
                  }}
                >
                  <Button
                    className="box-shadow-silver"
                    // className="box-shadow"
                    style={{
                      backgroundColor: '#fff',
                      color: '#000',
                      letterSpacing: '3px',
                      fontFamily: 'Open Sans',
                    }}
                  >
                    NEXT
                  </Button>
                </Link>}

              </center>
            </Grid.Column>
            <Grid.Column width={2} />
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}
