import React, { Component } from 'react';
import { Grid, Input, Button, Icon, Divider, Image, Dropdown } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

export default class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile:'',loginButtonStatus:true,
      inputdisable:false,
      Guest:false
    };
  }
mobileNumber(event,e){
  if(e.value.length == 9){
    this.setState({loginButtonStatus:false,inputdisable:true})
  }
  // console.log('mobile number',e.value);
  this.setState({mobile:e.value})

}
forGuest(){
  this.setState({Guest:true})
}
  render(){
    const options = [
      { key: 'nl', value: 'nl', flag: 'nl', text: '+31' },
    // { key: 'gb', value: 'gb', flag: 'gb', text: '+44' }

  ]
    return(
      <div
        className="bgImageSignin"
        style={{  }}
      >
        <Grid>

          <Grid.Row style={{ marginTop: '30%' }} only="mobile">
            <Grid.Column width={2} />
            <Grid.Column width={12}>
              <Input fluid
                type='number'
                disabled={this.state.inputdisable}
                label={<Dropdown defaultValue={options[0].value} options={options} />}
                labelPosition="left"
                placeholder="Mobile Number"
                onChange={this.mobileNumber.bind(this)}
              />
            </Grid.Column>
            <Grid.Column width={2} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={2} />
            <Grid.Column width={12}>
              {' '}
              <center>
                {/* <Link to=`/signup/${this.state.mobile}`> */}
                <Button disabled={this.state.loginButtonStatus}
                  as={Link} to={`/signup/${this.state.mobile}`}
                  fluid
                  style={{
                    borderRadius: '5px',
                    backgroundColor: '#cc0000',
                    letterSpacing: '2px',
                    fontWeight: 'bold',
                    textTransform: 'uppercase',
                    fontSize: '100%',
                    color: '#FAFAFA',
                  }}
                >
                  LOGIN / SIGN IN
                </Button>
              </center>
            </Grid.Column>
            <Grid.Column width={2} />
          </Grid.Row>
          <Grid.Row>

            <Grid.Column width={16}>
              <Link to={`/home/${this.state.Guest}`}>
              <h4 style={{textAlign: 'center',fontFamily: 'Raleway',letterSpacing: '2px',color:'black'}} onClick={this.forGuest.bind(this)}>
                Continue as a Guest
              </h4>
              </Link>
            </Grid.Column>

          </Grid.Row>
          <Grid.Row style={{ marginTop: '-2%' }} only="mobile">
            <Grid.Column width={1} />
            <Grid.Column
              width={14}
              style={{
                textAlign: 'center',
                textTransform: 'capitalize',
                letterSpacing: '2px',
                fontFamily: 'Raleway',
              }}
            >
              <Divider horizontal style={{ color: 'black' }}>
                Or
              </Divider>
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={6} />
            <Grid.Column width={6}>
              <h4>Continue with</h4>
            </Grid.Column>
            <Grid.Column width={6} />
          </Grid.Row>
          <Grid.Row style={{ marginTop: '-2%' }} only="mobile">
            <Grid.Column width={3} />
            <Grid.Column width={10}>
              <Link to="/fb">
                <Button
                  fluid
                  color="facebook"
                  style={{ letterSpacing: '2px', borderRadius: '25px' }}
                >
                  <Icon name="facebook" style={{ float: 'left' }} />FACEBOOK
                </Button>
              </Link>
            </Grid.Column>
            <Grid.Column width={3} />
          </Grid.Row>

          <Grid.Row style={{ marginTop: '-3%' }} only="mobile">
            <Grid.Column width={3} />
            <Grid.Column width={10}>
              <Button
                fluid
                color="google plus"
                style={{ letterSpacing: '2px', borderRadius: '25px' }}
              >
                <Icon name="google" style={{ float: 'left' }} />GOOGLE
              </Button>
            </Grid.Column>
            <Grid.Column width={3} />
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={1} />
            <Grid.Column width={1}>
              <Icon name="lock" />
            </Grid.Column>
            <Grid.Column width={13}>
              <p style={{ fontSize: '13px', fontFamily: 'Source Sans Pro sans serif' }}>
                We'll never post anything without your permission.
              </p>
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}
