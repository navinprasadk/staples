import React, { Component } from "react";
import { Dropdown, Grid, TextArea, Form, Button } from "semantic-ui-react";
import { Link } from "react-router-dom";
import AppBar from "./AppBar.jsx";

export default class ServiceDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cleaningType: "",
      numberOfFloors: null,
      numberOfRooms: null,
      numberOfBathroom: null,
      area: null,
      address: ""
    };
  }

  render() {
    const cleaningTypeOptions = [
      {
        text: "Full Cleaning",
        key: "Full Cleaning",
        value: "Full Cleaning"
      },
      {
        text: "Deep Cleaning",
        key: "Deep Cleaning",
        value: "Deep Cleaning"
      },
      {
        text: "Complete Cleaning",
        key: "Complete Cleaning",
        value: "Complete Cleaning"
      }
    ];
    const numberOptions = [
      {
        text: "1",
        value: "1"
      },
      {
        text: "2",
        value: "2"
      },
      {
        text: "3",
        value: "3"
      },
      {
        text: "4",
        value: "4"
      },
      {
        text: "5",
        value: "5"
      },
      {
        text: "6",
        value: "6"
      }
    ];
    const areaOptions = [
      {
        text: "less than 1000",
        value: "less than 1000"
      },
      {
        text: "1000 to 2000",
        value: "1000 to 2000"
      },
      {
        text: "2000 to 3000 ",
        value: "2000 to 3000"
      },
      {
        text: "3000 to 4000",
        value: "3000 to 4000"
      },
      {
        text: "4000 to 5000 ",
        value: "4000 to 5000"
      }
    ];
    return (
      <div className="container">
        <AppBar />
        <Grid>
          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6}>Type</Grid.Column>
            <Grid.Column width={8}>
              <Dropdown
                placeholder="Select Cleaning Type"
                selection
                fluid
                options={cleaningTypeOptions}
                value={this.state.cleaningType}
                onChange={(e, selected) => {
                  this.setState({ cleaningType: selected.value });
                }}
              />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6}>Floors</Grid.Column>
            <Grid.Column width={8}>
              <Dropdown
                placeholder="Number of Floors"
                selection
                fluid
                options={numberOptions}
                value={this.state.numberOfFloors}
                onChange={(e, selected) => {
                  this.setState({ numberOfFloors: selected.value });
                }}
              />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6}>Rooms</Grid.Column>
            <Grid.Column width={8}>
              <Dropdown
                placeholder="Number of Rooms"
                selection
                fluid
                options={numberOptions}
                value={this.state.numberOfRooms}
                onChange={(e, selected) => {
                  this.setState({ numberOfRooms: selected.value });
                }}
              />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6}>Bathroom</Grid.Column>
            <Grid.Column width={8}>
              <Dropdown
                placeholder="Number of Bathroom"
                selection
                fluid
                options={numberOptions}
                value={this.state.numberOfBathroom}
                onChange={(e, selected) => {
                  this.setState({ numberOfBathroom: selected.value });
                }}
              />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6}>Area in sq.ft</Grid.Column>
            <Grid.Column width={8}>
              <Dropdown
                placeholder="less than 1000"
                selection
                fluid
                options={areaOptions}
                value={this.state.area}
                onChange={(e, selected) => {
                  this.setState({ area: selected.value });
                }}
              />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6}>Select the Date</Grid.Column>
            <Grid.Column width={8}>
              <input type="date" />
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={1} />
            <Grid.Column width={6}>Address</Grid.Column>
            <Grid.Column width={8}>
              <Form>
                {" "}
                <TextArea />
              </Form>
            </Grid.Column>
            <Grid.Column width={1} />
          </Grid.Row>

          <Grid.Row only="mobile">
            <Grid.Column width={2} />

            <Grid.Column width={12}>
              <center>
                <Link
                  to={{
                    pathname: "/serviceConfirmation",
                    state: {
                      cleaningType: this.state.cleaningType,
                      numberOfFloors: this.state.numberOfFloors,
                      numberOfRooms: this.state.numberOfRooms,
                      numberOfBathroom: this.state.numberOfBathroom,
                      area: this.state.area,
                      address: this.state.address
                    }
                  }}
                >
                  <Button
                    style={{
                      backgroundColor: "#000",
                      color: "#fff",
                      letterSpacing: "2px"
                    }}
                  >
                    SUBMIT
                  </Button>
                </Link>
              </center>
            </Grid.Column>
            <Grid.Column width={2} />
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}
//  const { foo } = props.location.state;
{
  /* <Link to={{ pathname: "/route", state: { foo: "bar" } }}>My route</Link>; */
}
