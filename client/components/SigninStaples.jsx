import React from 'react';
import { Grid, Input, Button, Icon, Divider, Image } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const SigninStaples = () => (
  <div
    style={{
      // backgroundImage: "url('./client/Images/bg-red.jpg')",
      overflow: 'hidden',
      height: '100vh',
    }}
  >
    <Grid>
      {/* backgroundImage:'url(https://www298.lunapic.com/do-not-link-here-use-hosting-instead/151800841630185068?1589511572)' */}
      <Grid.Row style={{ marginTop: '30%' }} only="mobile">
        <Grid.Column width={2} />
        <Grid.Column
          width={12}
          style={{
            fontWeight: 'normal',
            letterSpacing: '3px',
            textAlign: 'center',
          }}
        >
          {/* <h1 className="titleText" styles={{ color: '#fff' }}>
            STAPLES
          </h1> */}
          <center>
            <Image
              size="small"
              src="./client/Images/logo.png"
             //src="http://logos-download.com/wp-content/uploads/2016/12/Staples_logo_logotype.png"
            />
          </center>
        </Grid.Column>
        <Grid.Column width={2} />
      </Grid.Row>

      <Grid.Row style={{ marginTop: '5%' }} only="mobile">
        <Grid.Column width={2} />
        <Grid.Column
          width={12}
          style={{
            fontWeight: 'normal',
            letterSpacing: '3px',
            textAlign: 'center',
          }}
        >
          <h3 className="titleText" style={{ marginTop: '5%' }}>
            Admin Login
          </h3>
        </Grid.Column>
        <Grid.Column width={2} />
      </Grid.Row>

      <Grid.Row style={{ marginTop: '0%' }} only="mobile">
        <Grid.Column width={2} />
        <Grid.Column width={12}>
          <Input fluid icon="user" iconPosition="left" placeholder="USER NAME" />
        </Grid.Column>
        <Grid.Column width={2} />
      </Grid.Row>

      <Grid.Row style={{ marginTop: '-1%' }} only="mobile">
        <Grid.Column width={2} />
        <Grid.Column width={12}>
          <Input
            fluid
            icon="lock"
            iconcolor="blue"
            iconPosition="left"
            type="password"
            placeholder="PASSWORD"
          />
        </Grid.Column>
        <Grid.Column width={2} />
      </Grid.Row>

      <Grid.Row style={{ marginBottom: '10%' }} only="mobile">
        <Grid.Column width={4} />
        <Grid.Column width={8}>
          {' '}
          <center>
            <Link to="/ordersearch">
              <Button
                fluid
                className="box-shadow"
                style={{
                  borderRadius: '5px',
                  letterSpacing: '3px',
                  fontWeight: 'bold',
                  textTransform: 'uppercase',
                  fontSize: '100%',
                  color: '#FAFAFA',
                  // color: '#fff',
                  fontFamily: 'Open Sans',
                }}
              >
                SIGN IN
              </Button>
            </Link>
          </center>
        </Grid.Column>
        <Grid.Column width={4} />
      </Grid.Row>
    </Grid>
  </div>
);
export default SigninStaples;
