// import React, { Component } from 'react';
// import { Dropdown, Grid, Input } from 'semantic-ui-react';
//
// // list of categories for dropdown
// const categoryOptions = [
//   {
//     text: 'AC Maintenance',
//     key: 'AC Maintenance',
//     value: 'AC Maintenance',
//   },
//   {
//     text: 'Deep Cleaning',
//     key: 'Deep Cleaning',
//     value: 'Deep Cleaning',
//   },
//   {
//     text: 'Full Cleaning',
//     key: 'Full Cleaning',
//     value: 'Full Cleaning',
//   },
// ];
//
// // List of options for each category
// const acMaintenance = ['AreaInSqFt'];
// const deepCleaning = ['Bathroom', 'Room', 'AdditionalSqFt'];
// const fullCleaning = ['Bathroom', 'Pantry', 'Room', 'Window', 'AdditionalSqFt'];
//
// // Internal Styling
// const inputLabelStyle = {
//   fontFamily: 'Open Sans',
//   textTransform: 'capitalise',
//   fontSize: 14,
//   color: 'white',
//   fontWeight: '600',
// };
//
// export default class CategoryForVendorRegistration extends Component {
//   constructor(props) {
//     super(props);
//     this.handleCategoryChange = this.handleCategoryChange.bind(this);
//     this.handleValueChange = this.handleValueChange.bind(this);
//   }
//
//   handleCategoryChange(e, { value }) {
//     this.props.handleCategoryChange(value);
//   }
//
//   handleValueChange(value, index, fieldName) {
//     this.props.handleValueChange(value, index, fieldName);
//   }
//
//   render() {
//     console.log('props: ', this.props);
//     const { index, selectedCategories } = this.props;
//     console.log('cat: ', selectedCategories[index].category);
//     return (
//       <Grid style={{ overflow: 'hidden' }}>
//         <Grid.Row only="mobile">
//           <Grid.Column width={1} />
//           <Grid.Column width={6} style={inputLabelStyle}>
//             Category
//           </Grid.Column>
//           <Grid.Column width={8}>
//             <Dropdown
//               placeholder="Select Category"
//               options={categoryOptions}
//               selection
//               fluid
//               onChange={this.handleCategoryChange}
//               value={selectedCategories[index].category}
//             />
//           </Grid.Column>
//           <Grid.Column width={1} />
//         </Grid.Row>
//         {selectedCategories[index].category === 'AC Maintenance'
//           ? acMaintenance.map(item =>
//                 (item === 'AreaInSqFt' ? (
//                   <Grid.Row only="mobile">
//                     <Grid.Column width={1} />
//                     <Grid.Column width={6} style={inputLabelStyle}>
//                       Cost per 500Sq.Ft
//                     </Grid.Column>
//                     <Grid.Column width={8}>
//                       <Input
//                         type="number"
//                         label="€"
//                         placeholder="Cost"
//                         fluid
//                         onChange={(e) => {
//                           this.handleValueChange(e.target.value, index, item);
//                         }}
//                       />
//                     </Grid.Column>
//                     <Grid.Column width={1} />
//                   </Grid.Row>
//                 ) : (
//                   <Grid.Row only="mobile">
//                     <Grid.Column width={1} />
//                     <Grid.Column width={6} style={inputLabelStyle}>
//                       {item}
//                     </Grid.Column>
//                     <Grid.Column width={8}>
//                       <Input
//                         type="number"
//                         label="€"
//                         placeholder={item}
//                         fluid
//                         onChange={(e) => {
//                           this.handleValueChange(e.target.value, index, item);
//                         }}
//                       />
//                     </Grid.Column>
//                     <Grid.Column width={1} />
//                   </Grid.Row>
//                 )))
//           : selectedCategories[index].category === 'Deep Cleaning'
//             ? deepCleaning.map(item =>
//                   (item === 'AdditionalSqFt' ? (
//                     <Grid.Row only="mobile">
//                       <Grid.Column width={1} />
//                       <Grid.Column width={6} style={inputLabelStyle}>
//                         Additional Sq.Ft
//                       </Grid.Column>
//                       <Grid.Column width={8}>
//                         <Input
//                           type="number"
//                           label="€"
//                           placeholder={item}
//                           fluid
//                           onChange={(e) => {
//                             this.handleValueChange(e.target.value, index, item);
//                           }}
//                         />
//                       </Grid.Column>
//                       <Grid.Column width={1} />
//                     </Grid.Row>
//                   ) : (
//                     <Grid.Row only="mobile">
//                       <Grid.Column width={1} />
//                       <Grid.Column width={6} style={inputLabelStyle}>
//                         {item}
//                       </Grid.Column>
//                       <Grid.Column width={8}>
//                         <Input
//                           type="number"
//                           label="€"
//                           placeholder={item}
//                           fluid
//                           onChange={(e) => {
//                             this.handleValueChange(e.target.value, index, item);
//                           }}
//                         />
//                       </Grid.Column>
//                       <Grid.Column width={1} />
//                     </Grid.Row>
//                   )))
//             : fullCleaning.map(item =>
//                   (item === 'AdditionalSqFt' ? (
//                     <Grid.Row only="mobile">
//                       <Grid.Column width={1} />
//                       <Grid.Column width={6} style={inputLabelStyle}>
//                         Additional Sq.Ft
//                       </Grid.Column>
//                       <Grid.Column width={8}>
//                         <Input
//                           type="number"
//                           label="€"
//                           placeholder={item}
//                           fluid
//                           onChange={(e) => {
//                             this.handleValueChange(e.target.value, index, item);
//                           }}
//                         />
//                       </Grid.Column>
//                       <Grid.Column width={1} />
//                     </Grid.Row>
//                   ) : (
//                     <Grid.Row only="mobile">
//                       <Grid.Column width={1} />
//                       <Grid.Column width={6} style={inputLabelStyle}>
//                         {item}
//                       </Grid.Column>
//                       <Grid.Column width={8}>
//                         <Input
//                           type="number"
//                           label="€"
//                           placeholder={item}
//                           fluid
//                           onChange={(e) => {
//                             this.handleValueChange(e.target.value, index, item);
//                           }}
//                         />
//                       </Grid.Column>
//                       <Grid.Column width={1} />
//                     </Grid.Row>
//                   )))}
//       </Grid>
//     );
//   }
// }


import React, { Component } from 'react';
import { Dropdown, Grid, Input } from 'semantic-ui-react';

// list of categories for dropdown
const categoryOptions = [
  {
    text: 'Duct Cleaning',
    key: 'Duct Cleaning',
    value: 'Duct Cleaning',
  },
  {
    text: 'Deep Cleaning',
    key: 'Deep Cleaning',
    value: 'Deep Cleaning',
  },
  {
    text: 'Full Cleaning',
    key: 'Full Cleaning',
    value: 'Full Cleaning',
  },
];

// List of options for each category
const acMaintenance = ['AreaInSqFt'];
const deepCleaning = ['Bathroom', 'Room', 'AdditionalSqFt'];
const fullCleaning = ['Bathroom', 'Pantry', 'Room', 'Window', 'AdditionalSqFt'];

// Internal Styling
const inputLabelStyle = {
  fontFamily: 'Open Sans',
  textTransform: 'capitalise',
  fontSize: 14,
  color: 'white',
  fontWeight: '600',
};

export default class CategoryForVendorRegistration extends Component {
  constructor(props) {
    super(props);
    this.handleCategoryChange = this.handleCategoryChange.bind(this);
    this.handleValueChange = this.handleValueChange.bind(this);
  }

  handleCategoryChange(e, { value }) {
    this.props.handleCategoryChange(value);
  }

  handleValueChange(value, index, fieldName) {
    this.props.handleValueChange(value, index, fieldName);
  }

  render() {
    console.log('props: ', this.props);
    const { index, selectedCategories } = this.props;
    console.log('cat: ', selectedCategories[index].category);
    return (
      <Grid style={{ overflow: 'hidden' }}>
        <Grid.Row only="mobile">
          <Grid.Column width={1} />
          <Grid.Column width={6} style={inputLabelStyle}>
            Category
          </Grid.Column>
          <Grid.Column width={8}>
            <Dropdown
              placeholder="Select Category"
              options={categoryOptions}
              selection
              fluid
              onChange={this.handleCategoryChange}
              value={selectedCategories[index].category}
            />
          </Grid.Column>
          <Grid.Column width={1} />
        </Grid.Row>
        {selectedCategories[index].category === 'Duct Cleaning'
          ? acMaintenance.map(item =>
                (item === 'AreaInSqFt' ? (
                  <Grid.Row only="mobile">
                    <Grid.Column width={1} />
                    <Grid.Column width={6} style={inputLabelStyle}>
                      Cost per 500Sq.Ft
                    </Grid.Column>
                    <Grid.Column width={8}>
                      <Input
                        type="number"
                        label="€"
                        placeholder="Cost"
                        fluid
                        onChange={(e) => {
                          this.handleValueChange(e.target.value, index, item);
                        }}
                      />
                    </Grid.Column>
                    <Grid.Column width={1} />
                  </Grid.Row>
                ) : (
                  <Grid.Row only="mobile">
                    <Grid.Column width={1} />
                    <Grid.Column width={6} style={inputLabelStyle}>
                      {item}
                    </Grid.Column>
                    <Grid.Column width={8}>
                      <Input
                        type="number"
                        label="€"
                        placeholder={item}
                        fluid
                        onChange={(e) => {
                          this.handleValueChange(e.target.value, index, item);
                        }}
                      />
                    </Grid.Column>
                    <Grid.Column width={1} />
                  </Grid.Row>
                )))
          : selectedCategories[index].category === 'Deep Cleaning'
            ? deepCleaning.map(item =>
                  (item === 'AdditionalSqFt' ? (
                    <Grid.Row only="mobile">
                      <Grid.Column width={1} />
                      <Grid.Column width={6} style={inputLabelStyle}>
                        Additional Sq.Ft
                      </Grid.Column>
                      <Grid.Column width={8}>
                        <Input
                          type="number"
                          label="€"
                          placeholder={item}
                          fluid
                          onChange={(e) => {
                            this.handleValueChange(e.target.value, index, item);
                          }}
                        />
                      </Grid.Column>
                      <Grid.Column width={1} />
                    </Grid.Row>
                  ) : (
                    <Grid.Row only="mobile">
                      <Grid.Column width={1} />
                      <Grid.Column width={6} style={inputLabelStyle}>
                        {item}
                      </Grid.Column>
                      <Grid.Column width={8}>
                        <Input
                          type="number"
                          label="€"
                          placeholder={item}
                          fluid
                          onChange={(e) => {
                            this.handleValueChange(e.target.value, index, item);
                          }}
                        />
                      </Grid.Column>
                      <Grid.Column width={1} />
                    </Grid.Row>
                  )))
            : fullCleaning.map(item =>
                  (item === 'AdditionalSqFt' ? (
                    <Grid.Row only="mobile">
                      <Grid.Column width={1} />
                      <Grid.Column width={6} style={inputLabelStyle}>
                        Additional Sq.Ft
                      </Grid.Column>
                      <Grid.Column width={8}>
                        <Input
                          type="number"
                          label="€"
                          placeholder={item}
                          fluid
                          onChange={(e) => {
                            this.handleValueChange(e.target.value, index, item);
                          }}
                        />
                      </Grid.Column>
                      <Grid.Column width={1} />
                    </Grid.Row>
                  ) : (
                    <Grid.Row only="mobile">
                      <Grid.Column width={1} />
                      <Grid.Column width={6} style={inputLabelStyle}>
                        {item}
                      </Grid.Column>
                      <Grid.Column width={8}>
                        <Input
                          type="number"
                          label="€"
                          placeholder={item}
                          fluid
                          onChange={(e) => {
                            this.handleValueChange(e.target.value, index, item);
                          }}
                        />
                      </Grid.Column>
                      <Grid.Column width={1} />
                    </Grid.Row>
                  )))}
      </Grid>
    );
  }
}
