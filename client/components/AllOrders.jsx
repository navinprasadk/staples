import React, { Component } from 'react';
import { Grid, Input, Button, Icon, Menu, Form, Dropdown,List, Divider } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import SwipeableViews from 'react-swipeable-views';
import AppBar from './AppBarhome.jsx';
import Request from 'superagent';

export default class CompanySearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index:'',
      activeItem:'Submitted',
      searchbyorder:[]
    };
  }
  handleItemClick(e,{name}){
    if(name=='Submitted')
    this.setState({index:0,activeItem:name})

  else if(name=='In Progress')
  this.setState({index:1,activeItem:name})

else
this.setState({index:2,activeItem:name})
}

componentWillMount(){
  var context = this;
Request.get('/allworkorders')
            .then(function(res) {
              console.log("All work orders:",res.text)
              context.setState({searchbyorder: JSON.parse(res.text)})
            });
}
  render() {
    const { activeItem } = this.state;
    var orderid = this.state.searchbyorder.map((item) =>{
        if(item.Status == 'Submitted'){
          return (
            <Grid.Row style={{borderRadius:'40px',backgroundColor:'#DFE5E3',marginBottom:'2%',marginLeft:'6%',marginRight:'6%'}}>
              <Grid.Column width={5}>
                <center><h5>{item.OrderId}</h5></center>
              </Grid.Column>
              <Grid.Column width={5}>
                <center>
                  <h5>{item.Submitted_date}</h5>
                </center>
              </Grid.Column>
              <Grid.Column width={5}>
                <center>
                  <h5>{item.Assignee}</h5>
                </center>
              </Grid.Column>
            </Grid.Row>
          )
        }

    })
    var orderid1 = this.state.searchbyorder.map((item) =>{
        if(item.Status == 'Inprogress'){
          return (
            <Grid.Row style={{borderRadius:'40px',backgroundColor:'#DFE5E3',marginBottom:'2%',marginLeft:'6%',marginRight:'6%'}}>
              <Grid.Column width={5}>
                <center><h5>{item.OrderId}</h5></center>
              </Grid.Column>
              <Grid.Column width={5}>
                <center>
                  <h5>{item.Submitted_date}</h5>
                </center>
              </Grid.Column>
              <Grid.Column width={5}>
                <center>
                  <h5>{item.Assignee}</h5>
                </center>
              </Grid.Column>
            </Grid.Row>
          )
        }

    })
    var orderid2 = this.state.searchbyorder.map((item) =>{
        if(item.Status == 'Completed'){
          return (
            <Grid.Row style={{borderRadius:'40px',backgroundColor:'#DFE5E3',marginBottom:'2%',marginLeft:'6%',marginRight:'6%'}}>
              <Grid.Column width={5}>
                <center><h5>{item.OrderId}</h5></center>
              </Grid.Column>
              <Grid.Column width={5}>
                <center>
                  <h5>{item.Submitted_date}</h5>
                </center>
              </Grid.Column>
              <Grid.Column width={5}>
                <center>
                  <h5>{item.Assignee}</h5>
                </center>
              </Grid.Column>
            </Grid.Row>
          )
        }

    })
    return (
      <div>
        <AppBar/>
        <Grid>
          <Grid.Row>
            <Grid.Column width={16}>
              <center>
                <h3>All Orders List</h3>
              </center>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Grid padded>
          <Grid.Row/>
        <Grid.Row>
          <Grid.Column width={1}/>
          <Grid.Column width={14}>
            <Menu pointing secondary>
          <Menu.Item name='Submitted' active={activeItem === 'Submitted'} onClick={this.handleItemClick.bind(this)} />
          <Menu.Item name='In Progress' active={activeItem === 'In Progress'} onClick={this.handleItemClick.bind(this)} />
          <Menu.Item name='Completed' active={activeItem === 'Completed'} onClick={this.handleItemClick.bind(this)} />
        </Menu>
        <SwipeableViews index={this.state.index} disabled>
          <div style={{height:'50vh'}}>
            <Grid >
                <Grid.Row style={{borderRadius:'40px',marginTop:'5%',marginLeft:'6%',marginRight:'6%'}}>
                  {/* <Grid.Column width={1}/> */}
                  <Grid.Column width={5}>
                    <center>
                      <h4 style={{fontWeight:'bold'}}>Order Id</h4>
                    </center>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <center>
                      <h4>Submitted Date</h4>
                    </center>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <center>
                      <h4>Assigned</h4>
                    </center>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={1}/>
                  <Grid.Column width={13}>
                    <Divider style={{backgroundColor:'black',marginTop:'-6%'}}/>
                  </Grid.Column>
                  <Grid.Column width={2}/>
                </Grid.Row>
                {orderid}
            </Grid>

          </div>
          <div style={{height:'50vh'}}>
            <Grid >
                <Grid.Row style={{borderRadius:'40px',marginTop:'5%',marginLeft:'6%',marginRight:'6%'}}>
                  {/* <Grid.Column width={1}/> */}
                  <Grid.Column width={5}>
                    <center>
                      <h4 style={{fontWeight:'bold'}}>Order Id</h4>
                    </center>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <center>
                      <h4>Submitted Date</h4>
                    </center>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <center>
                      <h4>Assigned</h4>
                    </center>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={1}/>
                  <Grid.Column width={13}>
                    <Divider style={{backgroundColor:'black',marginTop:'-6%'}}/>
                  </Grid.Column>
                  <Grid.Column width={2}/>
                </Grid.Row>
                {orderid1}
            </Grid>

          </div>
          <div style={{height:'50vh'}}>
            <Grid >
                <Grid.Row style={{borderRadius:'40px',marginTop:'5%',marginLeft:'6%',marginRight:'6%'}}>
                  {/* <Grid.Column width={1}/> */}
                  <Grid.Column width={5}>
                    <center>
                      <h4 style={{fontWeight:'bold'}}>Order Id</h4>
                    </center>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <center>
                      <h4>Submitted Date</h4>
                    </center>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <center>
                      <h4>Assigned</h4>
                    </center>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column width={1}/>
                  <Grid.Column width={13}>
                    <Divider style={{backgroundColor:'black',marginTop:'-6%'}}/>
                  </Grid.Column>
                  <Grid.Column width={2}/>
                </Grid.Row>
                {orderid2}
            </Grid>

          </div>

  </SwipeableViews>
        </Grid.Column>
        <Grid.Column width={1}/>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column width={16}>
            <center>
              <Button as={Link} to='/ordersearch'>Back</Button>
            </center>
          </Grid.Column>
        </Grid.Row>
</Grid>
      </div>
    );
  }
}
