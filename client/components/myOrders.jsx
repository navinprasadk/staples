import React, { Component } from "react";
import {
  Grid,
  Image,
  Header,
  Icon,
  Segment,
  Label
} from "semantic-ui-react";
import AppBarhome from './AppBarhome.jsx';
import Request from 'superagent';

export default class OrderDetails extends Component {
  constructor(props) {
    super(props);
    this.state={
      serviceOrder:[]
    }
  }
  componentDidMount(){
    Request.get('/serviceOrderDetails')
            .end((err, res)=>{
              if(err){
                console.log('Error from fetching bookingOrder data - >',err);
              } else{
                console.log('to check for service value',res.body);
                this.setState({serviceOrder:res.body})
              }
            })
  }
render(){
  console.log('to check fot service order status',this.state.serviceOrder);
  var myOrder = this.state.serviceOrder.map((item,i)=>{
    // console.log('..................>',item.);
    return(
      <Grid.Row>
        <Grid.Column width={1} />
        <Grid.Column width={14} style={{display: "flex",flexDirection: "row",justifyContent: "flex-start"}} >
          <Segment raised style={{width:'100%'}}>
          <Label as='a' color='blue' ribbon='right'>{item.so_id}</Label>
          <Header as='h2' style={{marginTop:'-7%'}}>{item.service_category_name}</Header>
          <h4 style={{marginTop:'-3%'}}>{item.customer_location}</h4>
          {item.vendor_name != null ? <div><h3>Allocated : <strong>{item.vendor_name}</strong></h3>
          <h3 style={{marginTop:'-3%'}}>Vendor Rating : <strong>{item.vendor_rating}</strong></h3></div> : <div><h3><center>Vendor will be allocated Soon.</center></h3></div> }

        </Segment>
        </Grid.Column>
      <Grid.Column width={1} />
      </Grid.Row>
    )
  })
  return(
    <div>
    <AppBarhome/>
      <Grid>
        {myOrder}
      {/* <Grid.Row>
        <Grid.Column width={1} />
        <Grid.Column width={14} style={{display: "flex",flexDirection: "row",justifyContent: "flex-start"}} >
          <Segment raised>
          <Label as='a' color='blue' ribbon='right'>123578</Label>
          <Header as='h2' style={{marginTop:'-7%'}}> Full Cleaning </Header>
          <h4 style={{marginTop:'-3%'}}>Wipro Technologies, Shikaripalya, Electronics City, Doddathoguru, Karnataka 560100</h4>
          <h3>Allocated to <strong>Mr.X</strong></h3>
          <h3 style={{marginTop:'-3%'}}>Contact No:<strong>0123456789</strong></h3>
        </Segment>
        </Grid.Column>
      <Grid.Column width={1} />
      </Grid.Row> */}
    </Grid>
    </div>
  );
 }
}
