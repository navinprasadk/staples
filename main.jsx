import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';

import AppRouter from './client/AppRouter.jsx';

injectTapEventPlugin();

ReactDOM.render(
  <MuiThemeProvider>
    <AppRouter />
  </MuiThemeProvider>,
  document.getElementById('reactApp'),
);
